<?php

namespace App\Http\Controllers\APIController;

use App\Http\Controllers\Controller;
use App\Http\Traits\System_Config;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

Class SystemConfigController extends Controller
{
	use System_Config;

	public function datatables_system_config($option)
    {
    	try
    	{
    		$this->is_system_config_option_exist($option);
	        if($option == 'announcement')
	        {
	        	$data = $this->get_announcement();
	        }
            elseif($option == 'quotation')
            {
                $data = $this->get_quotation();
            }
            elseif($option == 'multiple_training')
            {
                $data = $this->get_employee_trainings();
            }
	        else
	        {
	            $data = array();
	        }


            $datatables = Datatables::of($data)
            ->addColumn('action', function($data) use ($option) {

                $id = $data->id;

                if($option == 'quotation')
                {
                    return '<div class="tools"><button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button><div role="menu" class="dropdown-menu" x-placement="bottom-start"><a href="javascript:void(0);" class="dropdown-item" onclick="set_active('.$id.');"><i class="icon icon-left mdi mdi-notifications-active"></i>'.trans('page.set_active').'</a><a href="'.url('/system_config/'.$option.'/'.$id.'/edit').'" class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>'.trans('page.edit').'</a><a href="javascript:void(0);" class="dropdown-item" onclick="delete_record('.$id.');"><i class="icon icon-left mdi mdi-delete"></i>'.trans('page.delete').'</a></div></div>';
                }
                elseif ($option == 'multiple_training')
                {
                    return '<div class="tools"><button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button><div role="menu" class="dropdown-menu" x-placement="bottom-start"><a href="javascript:void(0);" class="dropdown-item" onclick="delete_record('.$id.');"><i class="icon icon-left mdi mdi-delete"></i>'.trans('page.delete').'</a></div></div>';
                }
                else
                {
                    return '<div class="tools"><button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button><div role="menu" class="dropdown-menu" x-placement="bottom-start"><a href="'.url('/system_config/'.$option.'/'.$id.'/edit').'" class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>'.trans('page.edit').'</a><a href="javascript:void(0);" class="dropdown-item" onclick="delete_record('.$id.');"><i class="icon icon-left mdi mdi-delete"></i>'.trans('page.delete').'</a></div></div>';
                }

            });

            $rawColumns = ['action'];
            if($option == 'quotation')
            {
                $rawColumns[] = 'quotation';

                $datatables->editColumn('quotation', function($data){
                    
                    if($data->status == 1)
                    {
                        return $data->quotation.'<div class="pull-right"><span class="badge badge-success"><span class="mdi mdi-check"></span>&nbspActive</span></div>';
                    }

                    return $data->quotation;
                });
            }

           

    	}
    	catch(Exception $e)
    	{
    		return response(['errors' => $e->getMessage()], 201);
    	}

        return $datatables->rawColumns($rawColumns)->make(true);
    }

    public function get_announcement_by($id)
    {
        try
        {
            $announcement = $this->check_exist_announcement($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['announcement' => $announcement]);
    }

    public function get_quotation_by($id)
    {
        try
        {
            $quotation = $this->check_exist_quotation($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['quotation' => $quotation]);
    }

    public function get_administrators()
    {
        try
        {
            $administrators = $this->get_user();
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['administrators' => $administrators, 'count_administrator' => count($administrators)]);
    }

    public function get_administrator_account_by($id)
    {
        try
        {
            $administrator = $this->check_exist_administrator_account($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['administrator' => $administrator]);
    }

    public function get_employee_account_by($id)
    {
        try
        {
            $employee = $this->check_exist_employee_account($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['employee' => $employee]);
    }

    public function filter_administrator_account_by(request $request)
    {
        try
        {
            $filter_value = $request->get('filter_value');

            $administrators = $this->filter_administrator_account($filter_value);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['administrators' => $administrators, 'count_administrator' => count($administrators)]);
    }
}