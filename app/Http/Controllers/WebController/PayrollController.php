<?php

namespace App\Http\Controllers\WebController;

use App\Http\Controllers\Controller;
use App\Http\Traits\Employee;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class PayrollController extends Controller
{
    use Employee;

    private $payroll_url;

    private $payroll_icon;

    private $input_employee_policy;

    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'payroll';

        $this->payroll_url = url('/payroll');

        $this->payroll_icon = 'icon mdi mdi-money-box';

        $this->employee_policy = array();
    }

    public function payroll(request $request, $option)
    {
        try
        {
            $this->is_payroll_option_exist($option);

            $data = ['module' => $this->module, 'option' => $option, 'icon' => $this->payroll_icon, 'payroll_url' => $this->payroll_url];

            if(in_array($option, ['employee_policy', 'loan_info', 'benefit_info', 'deduction_info']))
            {
                $view = 'payroll.employee_setup.index';

                if($option == 'employee_policy')
                {
                    $data = array_merge($data, [
                    'frm_action' => $this->payroll_url.'/'.$option.'/update',
                    'cancel_url' => $this->payroll_url.'/'.$option,
                    'title' => $option,
                    'list_employees' => $this->list_employees(),
                    'file' => 'payroll.employee_setup.employee_policy.form',
                    'list_banks' => $this->list_banks(),
                    'list_responsibilitycenters' => $this->list_responsibilitycenters(),
                    'list_gsispolicy' => $this->list_gsispolicy(),
                    'list_philhealthpolicy' => $this->list_philhealthpolicy(),
                    'list_pagibigpolicy' => $this->list_pagibigpolicy(),
                    ]);
                }
                elseif($option == 'loan_info')
                {
                    if($request->input('id')) $data['employee_id'] = $request->input('id');

                    $data = array_merge($data, [
                    'list_employees' => $this->list_employees(),
                    'title' => $option,
                    'file' => 'payroll.employee_setup.loan_info.table',
                    'default_table_id' => 'loan_info_tbl',
                    'default_columns' => array(
                        ['data' => 'action', 'sortable' => false],
                        ['data' => 'start_date', 'class' => 'text-center'],
                        ['data' => 'end_date', 'class' => 'text-center'],
                        ['data' => 'loan_name', 'class' => 'text-center'],
                    ),
                    'javascript_void' => $this->javascript_void,
                    ]);
                }
                elseif($option == 'benefit_info')
                {
                    if($request->input('id')) $data['employee_id'] = $request->input('id');

                    $data = array_merge($data, [
                    'list_employees' => $this->list_employees(),
                    'title' => $option,
                    'file' => 'payroll.employee_setup.benefit_info.table',
                    'default_table_id' => 'benefit_info_tbl',
                    'default_columns' => array(
                        ['data' => 'action', 'sortable' => false],
                        ['data' => 'effectivity_date', 'class' => 'text-center'],
                        ['data' => 'start_date', 'class' => 'text-center'],
                        ['data' => 'end_date', 'class' => 'text-center'],
                        ['data' => 'benefit_name', 'class' => 'text-center'],
                    ),
                    'javascript_void' => $this->javascript_void,
                    ]);
                }
                elseif($option == 'deduction_info')
                {
                    if($request->input('id')) $data['employee_id'] = $request->input('id');

                    $data = array_merge($data, [
                    'list_employees' => $this->list_employees(),
                    'title' => $option,
                    'file' => 'payroll.employee_setup.deduction_info.table',
                    'default_table_id' => 'deduction_info_tbl',
                    'default_columns' => array(
                        ['data' => 'action', 'sortable' => false],
                        ['data' => 'effectivity_date', 'class' => 'text-center'],
                        ['data' => 'start_date', 'class' => 'text-center'],
                        ['data' => 'end_date', 'class' => 'text-center'],
                        ['data' => 'deduction_name', 'class' => 'text-center'],
                    ),
                    'javascript_void' => $this->javascript_void,
                    ]);
                }
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }

        return view($view, $data);
    }

    public function create_payroll(request $request, $option)
    {
        try
        {
            $data = ['module' => $this->module, 'option' => $option, 'icon' => $this->payroll_icon, 'payroll_url' => $this->payroll_url];

            $this->is_payroll_option_exist($option);

            if($option == 'loan_info')
            {
                $id = $request->input('id');

                $employee = $this->check_exist_employee($id);

                $data = array_merge($data, [
                'employee' => $employee,
                'file' => 'payroll.employee_setup.loan_info.form',
                'emp_id' => $id,
                'list_loans' => $this->list_loans()
                ]);

                $file = 'payroll.employee_setup.create';
            }
            elseif($option == 'benefit_info')
            {
                $id = $request->input('id');

                $employee = $this->check_exist_employee($id);

                $data = array_merge($data, [
                'employee' => $employee,
                'file' => 'payroll.employee_setup.benefit_info.form',
                'emp_id' => $id,
                'list_benefits' => $this->list_benefits()
                ]);

                $file = 'payroll.employee_setup.create';
            }
            elseif($option == 'deduction_info')
            {
                $id = $request->input('id');

                $employee = $this->check_exist_employee($id);

                $data = array_merge($data, [
                'employee' => $employee,
                'file' => 'payroll.employee_setup.deduction_info.form',
                'emp_id' => $id,
                'list_deductions' => $this->list_deductions()
                ]);

                $file = 'payroll.employee_setup.create';
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }
        return view($file, $data);
    }

    public function store_payroll(request $request, $option)
    {
        try 
        {  
            $this->is_payroll_option_exist($option);
            
            if($option == 'loan_info')
            {
                $id = $request->get('emp_id');

                $this->check_exist_employee($id);

                $rules = [
                'start_date' => 'required',
                'end_date' => 'required',
                'loan' => 'required|in:'.implode(',', $this->get_value_by($this->list_loans(), 'id')),
                'date_granted' => 'required',
                'total_loan_amount' => 'required',
                'loan_balance' => 'required',
                'amortization' => 'required',
                //'date_terminated' => 'required',
                ];

                $this->validate_request($request->all(), $rules);

                $data = [
                'start_date' => $request->get('start_date'),
                'end_date' => $request->get('end_date'),
                'loan_id' => $request->get('loan'),
                'date_granted' => $request->get('date_granted'),
                'total_loan' => $request->get('total_loan_amount'),
                'loan_balance' => $request->get('loan_balance'),
                'amortization' => $request->get('amortization'),
                'date_terminated' => $request->get('date_terminated'),
                'employee_id' => $id,
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ];

                DB::beginTransaction();

                DB::table('employees_loan')
                ->insert($data);

                DB::commit();
            }
            elseif($option == 'benefit_info')
            {
                $id = $request->get('emp_id');

                $this->check_exist_employee($id);

                $rules = [
                'effective_date' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'benefit' => 'required|in:'.implode(',', $this->get_value_by($this->list_benefits(), 'id')),
                'description' => 'sometimes|nullable|max:255',
                'amount' => 'required'
                ];

                $this->validate_request($request->all(), $rules);

                $data = [
                'effectivity_date' => $request->get('effective_date'),
                'start_date' => $request->get('start_date'),
                'end_date' => $request->get('end_date'),
                'benefit_id' => $request->get('benefit'),
                'description' => $request->get('description'),
                'amount' => $request->get('amount'),
                'employee_id' => $id,
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ];

                DB::beginTransaction();

                DB::table('employees_benefit')
                ->insert($data);

                DB::commit();
            }
            elseif($option == 'benefit_info')
            {
                $id = $request->get('emp_id');

                $this->check_exist_employee($id);

                $rules = [
                'effective_date' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'deduction' => 'required|in:'.implode(',', $this->get_value_by($this->list_deductions(), 'id')),
                'description' => 'sometimes|nullable|max:255',
                'amount' => 'required'
                ];

                $this->validate_request($request->all(), $rules);

                $data = [
                'effectivity_date' => $request->get('effective_date'),
                'start_date' => $request->get('start_date'),
                'end_date' => $request->get('end_date'),
                'deduction_id' => $request->get('deduction'),
                'description' => $request->get('description'),
                'amount' => $request->get('amount'),
                'employee_id' => $id,
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ];

                DB::beginTransaction();

                DB::table('employees_deduction')
                ->insert($data);

                DB::commit();
            }
            elseif($option == 'deduction_info')
            {
                $id = $request->get('emp_id');

                $this->check_exist_employee($id);

                $rules = [
                'effective_date' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'deduction' => 'required|in:'.implode(',', $this->get_value_by($this->list_deductions(), 'id')),
                'description' => 'sometimes|nullable|max:255',
                'amount' => 'required'
                ];

                $this->validate_request($request->all(), $rules);

                $data = [
                'effectivity_date' => $request->get('effective_date'),
                'start_date' => $request->get('start_date'),
                'end_date' => $request->get('end_date'),
                'deduction_id' => $request->get('deduction'),
                'description' => $request->get('description'),
                'amount' => $request->get('amount'),
                'employee_id' => $id,
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ];

                DB::beginTransaction();

                DB::table('employees_deduction')
                ->insert($data);

                DB::commit();
            }
        } 
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }

        return response($this->success, 201); 
    }

    public function edit_payroll(request $request, $option, $id = null)
    {
        try
        {
            $data = ['module' => $this->module, 'option' => $option, 'icon' => $this->payroll_icon, 'payroll_url' => $this->payroll_url];

            $this->is_payroll_option_exist($option);

            if($option == 'loan_info')
            {
                $loan_info = $this->check_exist_payroll_loan_info($id);
                $employee = $this->check_exist_employee($loan_info->employee_id);

                $data = array_merge($data, [
                'employee' => $employee,
                'file' => 'payroll.employee_setup.loan_info.form',
                'item_id' => $id,
                'emp_id' => $loan_info->employee_id,
                'list_loans' => $this->list_loans()
                ]);

                $file = 'payroll.employee_setup.edit';
            }
            elseif($option == 'benefit_info')
            {
                $benefit_info = $this->check_exist_payroll_benefit_info($id);
                $employee = $this->check_exist_employee($benefit_info->employee_id);

                $data = array_merge($data, [
                'employee' => $employee,
                'file' => 'payroll.employee_setup.benefit_info.form',
                'item_id' => $id,
                'emp_id' => $benefit_info->employee_id,
                'list_benefits' => $this->list_benefits()
                ]);

                $file = 'payroll.employee_setup.edit';
            }
            elseif($option == 'deduction_info')
            {
                $deduction_info = $this->check_exist_payroll_deduction_info($id);
                $employee = $this->check_exist_employee($deduction_info->employee_id);

                $data = array_merge($data, [
                'employee' => $employee,
                'file' => 'payroll.employee_setup.deduction_info.form',
                'item_id' => $id,
                'emp_id' => $deduction_info->employee_id,
                'list_deductions' => $this->list_deductions()
                ]);

                $file = 'payroll.employee_setup.edit';
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }
        return view($file, $data);
    }

    public function update_payroll(request $request, $option, $id = null)
    {
        try 
        {  
            $this->is_payroll_option_exist($option);
            
            if($option == 'employee_policy')
            {
                $id = $request->get('emp_id');
                $name = $request->get('name');

                $this->check_exist_employee($name);

                $rules = [
                'bank' => 'required|in:'.implode(',', $this->get_value_by($this->list_banks(), 'id')),
                'account_no' => 'required|min:6|max:50',
                'responsibility_center' => 'required|in:'.implode(',', $this->get_value_by($this->list_responsibilitycenters(), 'id')),
                'gsis_policy' => 'required|in:'.implode(',', $this->get_value_by($this->list_gsispolicy(), 'id')),
                'philhealth_policy' => 'required|in:'.implode(',', $this->get_value_by($this->list_philhealthpolicy(), 'id')),
                'pagibig_policy' => 'required|in:'.implode(',', $this->get_value_by($this->list_pagibigpolicy(), 'id'))
                ];

                $this->validate_request($request->all(), $rules);

                $data = [
                'bank_id' => $request->get('bank'),
                'bank_account' => $request->get('account_no'),
                'responsibility_center_id' => $request->get('responsibility_center'),
                'gsis_policy_id' => $request->get('gsis_policy'),
                'philhealth_policy_id' => $request->get('philhealth_policy'),
                'pagibig_policy_id' => $request->get('pagibig_policy')
                ];

                if($this->count_payroll_employee_policy($id) == 0)
                {
                    DB::beginTransaction();

                    $data = array_merge($data, [
                    'employee_id' => $name,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                    ]);

                    DB::table('payroll_employee_policy')
                    ->insert($data);

                    DB::commit();
                }
                else
                {
                    DB::beginTransaction();

                    $data = array_merge($data, [
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                    ]);

                    DB::table('payroll_employee_policy')
                    ->where('payroll_employee_policy.employee_id', $id)
                    ->insert($data);

                    DB::commit(); 
                }
            }
            elseif($option == 'loan_info')
            {   
                $id = $request->get('item_id');

                $emp_id = $request->get('emp_id');

                $this->check_exist_employee($emp_id);

                $this->check_exist_payroll_loan_info($id);

                $rules = [
                'start_date' => 'required',
                'end_date' => 'required',
                'loan' => 'required|in:'.implode(',', $this->get_value_by($this->list_loans(), 'id')),
                'date_granted' => 'required',
                'total_loan_amount' => 'required',
                'loan_balance' => 'required',
                'amortization' => 'required',
                //'date_terminated' => 'required',
                ];

                $this->validate_request($request->all(), $rules);

                $data = [
                'start_date' => $request->get('start_date'),
                'end_date' => $request->get('end_date'),
                'loan_id' => $request->get('loan'),
                'date_granted' => $request->get('date_granted'),
                'total_loan' => $request->get('total_loan_amount'),
                'loan_balance' => $request->get('loan_balance'),
                'amortization' => $request->get('amortization'),
                'date_terminated' => $request->get('date_terminated'),
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
                ];

                DB::beginTransaction();

                DB::table('employees_loan')
                ->where('employees_loan.id', $id)
                ->update($data);

                DB::commit();
            }
            elseif($option == 'benefit_info')
            {   
                $id = $request->get('item_id');

                $emp_id = $request->get('emp_id');

                $this->check_exist_employee($emp_id);

                $this->check_exist_payroll_benefit_info($id);

                $rules = [
                'effective_date' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'benefit' => 'required|in:'.implode(',', $this->get_value_by($this->list_benefits(), 'id')),
                'description' => 'sometimes|nullable|max:255',
                'amount' => 'required'
                ];

                $this->validate_request($request->all(), $rules);

                $data = [
                'effectivity_date' => $request->get('effective_date'),
                'start_date' => $request->get('start_date'),
                'end_date' => $request->get('end_date'),
                'benefit_id' => $request->get('benefit'),
                'description' => $request->get('description'),
                'amount' => $request->get('amount'),
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
                ];

                DB::beginTransaction();

                DB::table('employees_benefit')
                ->where('employees_benefit.id', $id)
                ->update($data);

                DB::commit();
            }
            elseif($option == 'deduction_info')
            {   
                $id = $request->get('item_id');

                $emp_id = $request->get('emp_id');

                $this->check_exist_employee($emp_id);

                $this->check_exist_payroll_deduction_info($id);

                $rules = [
                'effective_date' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'deduction' => 'required|in:'.implode(',', $this->get_value_by($this->list_deductions(), 'id')),
                'description' => 'sometimes|nullable|max:255',
                'amount' => 'required'
                ];

                $this->validate_request($request->all(), $rules);

                $data = [
                'effectivity_date' => $request->get('effective_date'),
                'start_date' => $request->get('start_date'),
                'end_date' => $request->get('end_date'),
                'deduction_id' => $request->get('deduction'),
                'description' => $request->get('description'),
                'amount' => $request->get('amount'),
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
                ];

                DB::beginTransaction();

                DB::table('employees_deduction')
                ->where('employees_deduction.id', $id)
                ->update($data);

                DB::commit();
            }
        } 
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }

        return response($this->success, 201); 
    }

    public function delete_payroll(request $request, $option, $id = null)
    {
        try 
        {  
            $this->is_payroll_option_exist($option);
            
            if($option == 'loan_info')
            {
                DB::beginTransaction();

                DB::table('employees_loan')
                ->where('employees_loan.id', $id)
                ->update([
                'updated_by' => Auth::user()->id,
                'deleted_at' => DB::raw('now()')
                ]);

                DB::commit();
            }
            elseif($option == 'benefit_info')
            {
                DB::beginTransaction();

                DB::table('employees_benefit')
                ->where('employees_benefit.id', $id)
                ->update([
                'updated_by' => Auth::user()->id,
                'deleted_at' => DB::raw('now()')
                ]);

                DB::commit();
            }
            elseif($option == 'deduction_info')
            {
                DB::beginTransaction();

                DB::table('employees_deduction')
                ->where('employees_deduction.id', $id)
                ->update([
                'updated_by' => Auth::user()->id,
                'deleted_at' => DB::raw('now()')
                ]);

                DB::commit();
            }
        } 
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }

        return response($this->success, 201); 
    }
}