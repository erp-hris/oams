<?php

namespace App\Http\Controllers\WebController;

use App\Http\Controllers\Controller;
use App\Http\Traits\Dashboard;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class HomeController extends Controller
{
    use Dashboard;

    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'home';
    }

    public function index()
    {


        if(Auth::user()->pw_update == 0)
        {
            $this->module = 'account';

            $this->input_account = array(
                ['field_name' => '', 'input_name' => 'old_password'],
                ['field_name' => '', 'input_name' => 'new_password'],
                ['field_name' => '', 'input_name' => 'confirm_password'],
            );
            $data = [
                'module' => $this->module, 
                'option' => 'account',
                'title' => 'edit_account',
                'icon' => 'mdi mdi-account-circle',
                'cancel_url' => url('/account'),
                'file' => 'account.form',
                'frm_action' => url('/account/update'),
                'default_inputs' => $this->input_account
            ];

            return view('account.index', $data);
        }
        else
        {
            if(Auth::user()->level == 0)
            {
                $json_url = url('/attendance/employee/datatables');
                $columns = array(
                    ['data' => 'action', 'sortable' => false],
                    ['data' => 'last_name'],
                    ['data' => 'first_name'],
                    ['data' => 'employee_number'],
                    ['data' => 'department_name']
                );
                $file = 'attendance.daily-time-record.index';
                $data = ['module' => $this->module, 'option' => 'daily-time-record', 'json_url' => $json_url, 'columns' => $columns];

                return view($file, $data);
            }
            elseif (Auth::user()->level == 2) {
                $json_url = url('/attendance/employee/datatables');
                $columns = array(
                    ['data' => 'action', 'sortable' => false],
                    ['data' => 'last_name'],
                    ['data' => 'first_name'],
                    ['data' => 'employee_number'],
                    ['data' => 'department_name']
                );
                $file = 'users.index';
                $data = [
                    'module' => $this->module, 
                    'option' => 'user_account', 
                    'json_url' => $json_url, 
                    'columns' => $columns,
                    'option' => 'account',
                    'title' => 'user_account',
                    'icon' => 'mdi mdi-account-circle',
                    'file' => 'users.form',
                    'frm_action' => url('/user/store'),
                ];

                return view($file, $data);
            }
            else
            {
                $time_in        = '';
                $time_out       = '';
                $time_in_obj    = $this->get_time(Auth::user()->employee_id,'1',date('Y-m-d',time()));
                $time_out_obj   = $this->get_time(Auth::user()->employee_id,'4',date('Y-m-d',time()));
                if($time_in_obj)
                {
                    $time_in = $this->time_format($time_in_obj->attendance_time,2);
                }
                if($time_out_obj)
                {
                    $time_out = $this->time_format($time_out_obj->attendance_time,2);
                }

                $employee_data  = $this->get_employee_data(Auth::user()->employee_id);

                $file = 'home.index';
                $data = ['module' => $this->module, 'time_in' => $time_in, 'time_out' => $time_out, 'employee_id' => Auth::user()->employee_id, 'data' => $employee_data];

                return view($file, $data);
            }
        }
        
    }

    public function save_time(Request $request)
    {

        
        try {
            date_default_timezone_set("Asia/Manila");
            $hours    = $request->get('hours') * 60;
            $minutes  = $request->get('minutes') + $hours;
            $attendance_date = date('Y-m-d',time());
            
            $check = $this->check_logs(Auth::user()->employee_id, $request->get('entry_kind'), $attendance_date);

            if($check)
            {
                return response('existing', 200);
            }
            else
            {
                if($request->get('entry_kind') == 4)
                {
                    $check_time_in = $this->check_time_in(Auth::user()->employee_id, $attendance_date);

                    if($check_time_in)
                    {
                        DB::beginTransaction();
                        $insert_data = [
                            'employee_id' => Auth::user()->employee_id,
                            'attendance_date' => $attendance_date,
                            'attendance_time' => $minutes,
                            'entry_kind' => $request->get('entry_kind'),
                            'ip_address' => $request->ip(),
                            'created_by' => Auth::user()->id,
                            'created_at' => DB::raw('now()')
                        ];
                        DB::table('time_entry')
                        ->insert($insert_data);
                        DB::commit();  

                               
                        return response('success', 201);
                    }
                    else
                    {
                        return response('time_in_required', 201);
                    }
                }
                else
                {
                    DB::beginTransaction();
                    $insert_data = [
                        'employee_id' => Auth::user()->employee_id,
                        'attendance_date' => $attendance_date,
                        'attendance_time' => $minutes,
                        'ip_address' => $request->ip(),
                        'entry_kind' => $request->get('entry_kind'),
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                    ];
                    DB::table('time_entry')
                    ->insert($insert_data);
                    DB::commit();  


                                
                    return response('success', 201);
                }
                
                
            }

        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
    }

    public function attendance_logs($id, $month, $year)
    {
        $present_count              = 0;
        $restday_count              = 0;
        $leave_count                = 0;
        $no_lunch_count             = 0;
        $special_holiday_count      = 0;
        $legal_holiday_count        = 0;
        $undertime_count            = 0;
        $tardy_count                = 0;
        $absent_count               = 0;
        $total_tardy                = 0;
        $total_undertime            = 0;
        $tardy_count_per_week       = array(0,0,0,0,0,0,0);
        $tardy_hour_per_week        = array(0,0,0,0,0,0,0);
        $undertime_count_per_week   = array(0,0,0,0,0,0,0);
        $undertime_hour_per_week    = array(0,0,0,0,0,0,0);

        $dtr_logs           = array();
        $calendar_logs      = array();


        $curr_date          = date("Y-m-d",time());
        $num_of_days        = cal_days_in_month(CAL_GREGORIAN,$month,$year);
        $exact_end_days     = $num_of_days;
        $new_week           = '';
        $week_count         = 0;
        $month_start        = $year."-".$month."-01";
        $month_end          = $year."-".$month."-".$num_of_days;

        $work_schedule_name = '09:30 AM - 06:30 PM';

        if (strtotime($curr_date) <= strtotime($month_end))
        {
            $month_end = $curr_date;
        }
        else
        {
            $month_end = $month_end;
        }

        $num_of_days         = date("d",strtotime($month_end));
        $period              = $month."/01/".$year." - ".$month."/".$num_of_days."/".$year;

        for($d=1;$d<=$num_of_days;$d++)
        {
            if ($d <= 9) $d = "0".$d;
            $y      = $year."-".$month."-".$d;
            $temp_date = date("D",strtotime($y)).", ".$month."-".$d."-".$year;
            $dtr_date = date("D",strtotime($y)).", ".date('m-d-y',strtotime($y));
            $week   = date("W",strtotime($y));
            if ($week != $new_week) $week_count++;
            $dtr = [
                'attendance_date' => $y,
                'day' => $d,
                'day_name' => strtolower(date("l",strtotime($y))),
                'week' => $week_count,
                'entry_kind' => '',
                'time_in' => 0,
                'lunch_out' => 0,
                'lunch_in' => 0,
                'time_out' => 0,
                'office_authority_code' => '',
                'office_authority_name' => '',
                'office_authority_start_time' => 0,
                'office_authority_end_time' => 0,
                'office_authority_whole_day' => 0,
                'leave_code' => '',
                'leave_name' => '',
                'forced_leave' => 0,
                'holiday_name' => '',
                'holiday_type' => 0,
                'offsus_name' => '',
                'offsus_whole_day' => 0,
                'offsus_time_start' => 0,
                'offsus_required_time_in' => 0,
                'overtime_type' => '',
                'overtime_classification' => '',
                'overtime_time' => 0,
                'overtime_time_in' => 0,
                'overtime_time_out' => 0,
                'cto_hours' => 0
            ];
            $calendar = [
                'attendance_date' => $y,
                'temp_date' => $temp_date,
                'dtr_date' => $dtr_date,
                'day' => intval($d),
                'day_name' => strtolower(date("l",strtotime($y))),
                'week' => $week_count,
                'time_in' => 0,
                'lunch_out' => 0,
                'lunch_in' => 0,
                'time_out' => 0,
                'tardy' => 0,
                'undertime' => 0,
                'excess_time' => 0,
                'consume_time' => 0,
                'total_tardy_ut' => 0,
                'remarks' => ''
            ];


            $calendar_logs['summary'] = [
                'present_count' => 0,
                'restday_count' => 0,
                'leave_count' => 0,
                'no_lunch_count' => 0,
                'special_holiday_count' => 0,
                'legal_holiday_count' => 0,
                'undertime_count' => 0,
                'tardy_count' => 0,
                'absent_count' => 0,
                'total_tardy' => 0,
                'total_undertime' => 0,
                'tardy_equivalent' => 0,
                'undertime_equivalent' => 0
            ];


            $dtr_logs["attendance"][$y] = $dtr;
            $calendar_logs["attendance"][$y] = $calendar;
            $new_week = $week;
        }
        $bio_logs = $this->get_attendance($id, $month, $year);
        if($bio_logs)
        {
            foreach ($bio_logs as $key => $value) {
                $entry_kind = $value->entry_kind;
                $attendance_date = $value->attendance_date;
                $minutes = $value->attendance_time;
                if ($entry_kind == 1)
                {
                    $fld = 'time_in';
                }
                else if ($entry_kind == 2)
                {
                    $fld = 'lunch_out';
                }
                else if ($entry_kind == 3)
                {
                    $fld = 'lunch_in';
                }
                else if ($entry_kind == 4)
                {
                    $fld = 'time_out';
                }
                else
                {
                    $fld = '';
                }
                if ($fld != '')
                {
                    if (isset($dtr_logs["attendance"][$attendance_date]['attendance_date']))
                    {

                        if ($dtr_logs["attendance"][$attendance_date]['attendance_date'] == $attendance_date)
                        {
                            $dtr_logs["attendance"][$attendance_date][$fld] = $minutes;
                        }
                    }    
                }
            }
            
        }
        //dd($dtr_logs);
        if($bio_logs)
        {
            foreach ($dtr_logs as $key => $value) {
                foreach ($value as $nkey => $dtr) {
                    $tardy                          = 0;
                    $undertime                      = 0;
                    $consume_time                   = 0;
                    $excess_time                    = 0;
                    $remarks                        = '';
                    $attendance_date                = $dtr['attendance_date'];
                    $day                            = $dtr['day'];
                    $day_name                       = $dtr['day_name'];
                    $week                           = $dtr['week'];
                    $entry_kind                     = $dtr['entry_kind'];
                    $time_in                        = $dtr['time_in'];
                    $lunch_out                      = $dtr['lunch_out'];
                    $lunch_in                       = $dtr['lunch_in'];
                    $time_out                       = $dtr['time_out'];
                    $office_authority_code          = $dtr['office_authority_code'];
                    $office_authority_name          = $dtr['office_authority_name'];
                    $office_authority_start_time    = $dtr['office_authority_start_time'];
                    $office_authority_end_time      = $dtr['office_authority_end_time'];
                    $office_authority_whole_day     = $dtr['office_authority_whole_day'];
                    $leave_code                     = $dtr['leave_code'];
                    $leave_name                     = $dtr['leave_name'];
                    $forced_leave                   = $dtr['forced_leave'];
                    $holiday_name                   = $dtr['holiday_name'];
                    $holiday_type                   = $dtr['holiday_type'];
                    $offsus_name                    = $dtr['offsus_name'];
                    $offsus_whole_day               = $dtr['offsus_whole_day'];
                    $offsus_time_start              = $dtr['offsus_time_start'];
                    $offsus_required_time_in        = $dtr['offsus_required_time_in'];
                    $overtime_type                  = $dtr['overtime_type'];
                    $overtime_classification        = $dtr['overtime_classification'];
                    $overtime_time                  = $dtr['overtime_time'];
                    $overtime_time_in               = $dtr['overtime_time_in'];
                    $overtime_time_out              = $dtr['overtime_time_out'];
                    $cto_hours                      = $dtr['cto_hours'];

                    $data_time_in                   = '570';
                    $data_lunch_out                 = '0';
                    $data_lunch_in                  = '';
                    $data_time_out                  = '1110';
                    
                    if($time_in)
                    {
                        if($time_in > $data_time_in)
                        {
                            $tardy = $time_in - $data_time_in;
                            $tardy_count++;
                        }    
                    }
                    if ($time_out)
                    {
                        if($time_out < $data_time_out)
                        {
                            $undertime = $data_time_out - $time_out;
                            $undertime_count++;
                        }    
                    }

                    if(($time_in) || ($time_out)) 
                    {
                        $present_count++;
                        $remarks = '';
                        $lunch_out = '720';
                        $lunch_in = '780';
                        
                    }
                    if(($time_in) && ($time_out))
                    {
                        $working_hrs = ($time_out - $time_in) - 60;
                        if($working_hrs <= 300) $remarks = 'Absent';
                    }


                    
                    

                    if (isset($calendar_logs["attendance"][$attendance_date]['attendance_date']))
                    {
                        if ($calendar_logs["attendance"][$attendance_date]['attendance_date'] == $attendance_date)
                        {
                            $calendar_logs['attendance'][$attendance_date]['time_in']           = $this->time_format($dtr['time_in'],2);
                            $calendar_logs['attendance'][$attendance_date]['time_out']          = $this->time_format($dtr['time_out'],2);
                            $calendar_logs['attendance'][$attendance_date]['lunch_out']         = $this->time_format($lunch_out,2);
                            $calendar_logs['attendance'][$attendance_date]['lunch_in']          = $this->time_format($lunch_in,2);
                            $calendar_logs['attendance'][$attendance_date]['tardy']             = $this->time_format($tardy);
                            $calendar_logs['attendance'][$attendance_date]['undertime']         = $this->time_format($undertime);
                            $calendar_logs['attendance'][$attendance_date]['excess_time']       = $this->time_format($excess_time);
                            $calendar_logs['attendance'][$attendance_date]['consume_time']      = $this->time_format($consume_time);
                            $calendar_logs['attendance'][$attendance_date]['total_tardy_ut']    = $this->time_format($tardy + $undertime);

                            if(strtolower(date("D",strtotime($attendance_date))) != 'sat' && strtolower(date("D",strtotime($attendance_date))) != 'sun')
                            {
                                $calendar_logs['attendance'][$attendance_date]['remarks']    = $remarks;    

                                $current_date = date('Y-m-d',time());
                                $current_date = strtotime($current_date);
                                if(strtotime($attendance_date) != $current_date)
                                {
                                    if($time_in == '' || $time_out == '')
                                    {
                                        $calendar_logs['attendance'][$attendance_date]['remarks']    = 'Absent';
                                    }
                                }
                            }
                            else
                            {
                                $calendar_logs['attendance'][$attendance_date]['remarks']    = 'Restday';
                            }

                            
                            
                            

                            
                            $total_tardy += $tardy;
                            $total_undertime += $undertime;
                            
                        }   
                    }   
                }
            }

            for ($i=1; $i <= 6; $i++) { 
                $undertime_count += $undertime_count_per_week[$i];
                $tardy_count += $tardy_count_per_week[$i];
            }
            //dd($calendar_logs);
            $calendar_logs['summary'] = [
                'present_count' => $present_count,
                'restday_count' => $restday_count,
                'leave_count' => $leave_count,
                'no_lunch_count' => $no_lunch_count,
                'special_holiday_count' => $special_holiday_count,
                'legal_holiday_count' => $legal_holiday_count,
                'undertime_count' => $undertime_count,
                'tardy_count' => $tardy_count,
                'absent_count' => $absent_count,
                'total_tardy' => $this->time_format($total_tardy),
                'total_undertime' => $this->time_format($total_undertime),
                'tardy_equivalent' => '0',
                'undertime_equivalent' => '0',
                'work_schedule_name' => $work_schedule_name
            ];    
        }
        
        return $calendar_logs;
    }

    public function preview_dtr($id, $month, $year)
    {
        /*$month          = date('m',time());
        $year           = date('Y',time());*/
        $calendar_logs  = $this->attendance_logs($id, $month, $year);
        $file           = 'home.dtr-report';
        $month          = date("F",strtotime($year.'-'.$month.'-01'));
        $employee_id    = $id;
        $employee_data  = $this->get_employee_data($employee_id);
        return view($file, ['data' => $calendar_logs, 'month' => $month, 'year' => $year, 'employee' => $employee_data]);
    }


    public function show_employee_dtr()
    {
        $json_url = url('/attendance/employee/datatables');
        $columns = array(
            ['data' => 'action', 'sortable' => false],
            ['data' => 'last_name'],
            ['data' => 'first_name'],
            ['data' => 'employee_number'],
            ['data' => 'department_name']
        );
        $file = 'attendance.daily-time-record.index';
        $data = ['module' => $this->module, 'option' => 'daily-time-record', 'json_url' => $json_url, 'columns' => $columns];

        return view($file, $data);
    }

    public function list_employees_datatable()
    {
        if(Auth::user()->department_id)
        {
            $data = $this->get_employee_list(Auth::user()->department_id);
        }
        else
        {
            $data = $this->get_employee_list();    
        }
        
        $datatables = Datatables::of($data)
        ->addColumn('action', function($data) {
            if(Auth::user()->level == 2)
            {
                
                return '
                    <div class="tools text-center">
                        <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                            <a href="javascript:void(0);" class="dropdown-item" onclick="delete_employee('.$data->id.')">
                                    <i class="icon icon-left mdi mdi-delete"></i>&nbsp;Delete
                            </a>
                            <form
                            role="form"
                            method="POST"
                            action="'.url('/user/'.$data->id.'/delete').'" 
                            id="delete_emp_'.$data->id.'">'.
                            csrf_field().'
                            </form>

                            <a href="javascript:void(0);" class="dropdown-item" onclick="reset_pw('.$data->id.')">
                                    <i class="icon icon-left mdi mdi-key"></i>&nbsp;Reset PW
                            </a>
                            <a href="'.url('/user/'.$data->id.'/edit').'" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-edit"></i>&nbsp;Edit
                            </a>
                        </div>
                    </div>
                ';
            }
            else
            {
                return '<div class="tools text-center">
                    <button type="button" class="btn btn-space btn-success" onclick="viewDTR('.$data->id.')">
                        <i class="icon icon-left mdi mdi-eye"></i>View DTR
                    </button>
                </div>';    
            }
            
        });

        return $datatables
        ->rawColumns(['action'])
        ->make(true);
    }

    public function upload_accounts()
    {
        $data = $this->get_employee_list();
        if($data)
        {
            DB::beginTransaction();
            foreach ($data as $key => $value) {
                $employee_number = trim($value->employee_number);
                $username = $employee_number;
                $password = $employee_number.'123';
                $employee_id = $value->id;

                $insert_data = [
                    'name' => $username,
                    'username' => $username,
                    'employee_id' => $employee_id,
                    'password' => bcrypt(123456),
                    'level' => 1,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];
                DB::table('users')
                ->insert($insert_data);
            }
             DB::commit();
        }
    }

    public function upload_admin_account()
    {
        /*$users = array(
            ['username' => 'lmendoza', 'department_id' => '37', 'password' => '123456'],
            ['username' => 'iasetre', 'department_id' => '51', 'password' => '123456'],
            ['username' => 'acespaldon', 'department_id' => '34', 'password' => '123456'],
            ['username' => 'nraquino', 'department_id' => '47', 'password' => '123456'],
            ['username' => 'henal', 'department_id' => '50', 'password' => '123456'],
            ['username' => 'mtalvarez', 'department_id' => '53', 'password' => '123456'],
            ['username' => 'efrancisco', 'department_id' => '48', 'password' => '123456'],
            ['username' => 'rolgado', 'department_id' => '49', 'password' => '123456'],
            ['username' => 'rancheta', 'department_id' => '40', 'password' => '123456'],
            ['username' => 'cocampo', 'department_id' => '41', 'password' => '123456'],
            ['username' => 'acaganda', 'department_id' => '42', 'password' => '123456'],
            ['username' => 'raspuria', 'department_id' => '43', 'password' => '123456'],
        );

        DB::beginTransaction();
        foreach ($users as $key => $value) {
            $insert_data = [
                'name' => $value['username'],
                'username' => $value['username'],
                'employee_id' => '0',
                'password' => bcrypt(123456),
                'level' => 0,
                'department_id' => $value['department_id'],
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
            ];
            DB::table('users')
            ->insert($insert_data);
        }
        DB::commit();*/
        
        
    }

    public function user_account()
    {
        $json_url = url('/attendance/employee/datatables');
        $columns = array(
            ['data' => 'action', 'sortable' => false],
            ['data' => 'last_name'],
            ['data' => 'first_name'],
            ['data' => 'employee_number'],
            ['data' => 'department_name']
        );
        $file = 'users.index';
        $data = [
            'module' => $this->module, 
            'option' => 'user_account', 
            'json_url' => $json_url, 
            'columns' => $columns,
            'option' => 'account',
            'title' => 'user_account',
            'icon' => 'mdi mdi-account-circle',
            'file' => 'users.form',
            'frm_action' => url('/user/store'),
        ];

        return view($file, $data);
    }

    public function create_user()
    {
        $departments = $this->list_departments();
        $divisions   = $this->list_divisions();
        $sectors     = $this->list_sectors();
        $file = 'users.create';
        $data = [
            'module' => $this->module, 
            'option' => 'user_account', 
            'action_url' => url('user/store'), 
            'option' => 'account',
            'title' => 'user_account',
            'icon' => 'mdi mdi-account-circle',
            'file' => 'users.add_form',
            'departments' => $departments,
            'sectors' => $sectors,
            'divisions' => $divisions,
            'cancel_url' => url('user')
        ];

        return view($file, $data);
    }

    public function store_user(Request $request)
    {
        $check_if_exist = $this->check_employee_number($request->get('employee_number'));

        if($check_if_exist)
        {
            return response('exist', 201);
        }
        else
        {
            DB::beginTransaction();


            $insert_data = [
                'employee_number' => $request->get('employee_number'),
                'last_name' => $request->get('last_name'),
                'first_name' => $request->get('first_name'),
                'middle_name' => $request->get('middle_name'),
                'department_id' => $request->get('department_id'),
                'sector_id' => $request->get('sector_id'),
                'division_id' => $request->get('division_id'),
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
            ];
            DB::table('employees')
            ->insert($insert_data);


            $employee_id = $this->get_data_by($request->get('employee_number'));

            $insert_data = [
                'name' => $request->get('employee_number'),
                'username' => $request->get('employee_number'),
                'employee_id' => $employee_id->id,
                'password' => bcrypt(123456),
                'level' => 1,
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
            ];
            DB::table('users')
            ->insert($insert_data);


            DB::commit();

            return response('success', 201);
        }
        
    }

    public function delete_user($id)
    {
        $employee_id = $id;
        DB::beginTransaction();

        $check_in_employee_table = $this->check_employee($employee_id);
        if($check_in_employee_table)
        {
            DB::table('employees')->delete($employee_id);    
        }


        $check_in_user_table = $this->check_user($employee_id);
        if($check_in_user_table)
        {
            DB::table('users')
            ->where('employee_id', '=', $employee_id)
            ->delete();            
        }

        

        DB::commit();
        return response('success', 201);
    }

    public function reset_password($id)
    {
        DB::beginTransaction();

        DB::table('users')
        ->where('employee_id', '=', $id)
        ->update(
            [
                'password' => bcrypt(123456),
                'pw_update' => '0',
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
            ]
        );
        DB::commit();

        return response('success', 201);
    }

    public function update_user(Request $request, $id)
    {
        $check = $this->check_employee($id);
        if($check)
        {
            DB::beginTransaction();

            DB::table('employees')
            ->where('employees.id', '=', $id)
            ->update(
                [
                    'employee_number' => $request->get('employee_number'),
                    'last_name' => $request->get('last_name'),
                    'first_name' => $request->get('first_name'),
                    'middle_name' => $request->get('middle_name'),
                    'department_id' => $request->get('department_id'),
                    'sector_id' => $request->get('sector_id'),
                    'division_id' => $request->get('division_id'),
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ]
            );
            DB::commit();

            return response('success', 201);    
        }

        
    }


    public function edit_user($id)
    {
        $check = $this->check_employee($id);
        if($check)
        {
            $departments    = $this->list_departments();
            $divisions      = $this->list_divisions();
            $sectors        = $this->list_sectors();
            $file           = 'users.edit';
            $data = [
                'module' => $this->module, 
                'option' => 'user_account', 
                'action_url' => url('user/'.$id.'/update'), 
                'option' => 'account',
                'title' => 'user_account',
                'icon' => 'mdi mdi-account-circle',
                'file' => 'users.add_form',
                'departments' => $departments,
                'divisions' => $divisions,
                'sectors' => $sectors,
                'cancel_url' => url('user'),
                'data' => $this->get_employee_data($id)
            ];

            return view($file, $data);
        }
        else
        {

        }
    }



    public function admin_account()
    {
        $json_url = url('admin/datatables');
        $columns = array(
            ['data' => 'action', 'sortable' => false],
            ['data' => 'name'],
            ['data' => 'username']
        );
        $file = 'users.index';
        $data = [

            'module' => $this->module, 
            'option' => 'admin_account', 
            'json_url' => $json_url, 
            'columns' => $columns,
            'option' => 'account',
            'title' => 'admin_account',
            'icon' => 'mdi mdi-account-circle',
            'file' => 'admins.form',
            'frm_action' => url('/admin/store'),
        ];
        return view($file, $data);
    }

    public function list_admin_datatable()
    {
        $data = $this->list_users();       
        $datatables = Datatables::of($data)
        ->addColumn('action', function($data) {
            return '
                <div class="tools text-center">
                    <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                    <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                        <a href="javascript:void(0);" class="dropdown-item" onclick="delete_employee('.$data->id.')">
                                <i class="icon icon-left mdi mdi-delete"></i>&nbsp;Delete
                        </a>
                        <form
                        role="form"
                        method="POST"
                        action="'.url('/user/'.$data->id.'/delete').'" 
                        id="delete_emp_'.$data->id.'">'.
                        csrf_field().'
                        </form>

                        <a href="javascript:void(0);" class="dropdown-item" onclick="reset_pw('.$data->id.')">
                                <i class="icon icon-left mdi mdi-key"></i>&nbsp;Reset PW
                        </a>
                        <a href="'.url('/admin/'.$data->id.'/edit').'" class="dropdown-item">
                                <i class="icon icon-left mdi mdi-edit"></i>&nbsp;Edit
                        </a>
                    </div>
                </div>
            ';
        });

        return $datatables
        ->rawColumns(['action'])
        ->make(true);
    }

    public function edit_admin($id)
    {
        $check = $this->check_user_account($id);
        if($check)
        {
            $departments    = $this->list_departments();
            $divisions      = $this->list_divisions();
            $sectors        = $this->list_sectors();
            $file           = 'admins.edit';
            $data = [
                'module' => $this->module, 
                'option' => 'admin_account', 
                'action_url' => url('admin/'.$id.'/update'), 
                'option' => 'account',
                'title' => 'admin_account',
                'icon' => 'mdi mdi-account-circle',
                'file' => 'admins.update_form',
                'departments' => $departments,
                'divisions' => $divisions,
                'sectors' => $sectors,
                'cancel_url' => url('admin'),
                'data' => $this->get_user_data($id)
            ];

            return view($file, $data);
        }
        else
        {

        }
    }

    public function update_admin(Request $request, $id)
    {
        $check = $this->check_user_account($id);
        if($check)
        {
            DB::beginTransaction();

            DB::table('users')
            ->where('users.id', '=', $id)
            ->update(
                [
                    'name' => $request->get('name'),
                    'username' => $request->get('username'),
                    'department_id' => $request->get('department_id'),
                    'sector_id' => $request->get('sector_id'),
                    'division_id' => $request->get('division_id'),
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ]
            );
            DB::commit();

            return response('success', 201);    
        }

        
    }

    public function create_admin()
    {
        $departments = $this->list_departments();
        $divisions   = $this->list_divisions();
        $sectors     = $this->list_sectors();
        $file = 'admins.create';
        $data = [
            'module' => $this->module, 
            'option' => 'admin_account', 
            'action_url' => url('admin/store'), 
            'option' => 'account',
            'title' => 'admin_account',
            'icon' => 'mdi mdi-account-circle',
            'file' => 'admins.add_form',
            'departments' => $departments,
            'sectors' => $sectors,
            'divisions' => $divisions,
            'cancel_url' => url('admin')
        ];

        return view($file, $data);
    }


    public function store_admin(Request $request)
    {
        DB::beginTransaction();

        $insert_data = [
            'name' => $request->get('name'),
            'username' => $request->get('username'),
            'employee_id' => 0,
            'password' => bcrypt(123456),
            'level' => 0,
            'department_id' => $request->get('department_id'),
            'division_id' => $request->get('division_id'),
            'sector_id' => $request->get('sector_id'),
            'created_by' => Auth::user()->id,
            'created_at' => DB::raw('now()')
        ];
        DB::table('users')
        ->insert($insert_data);

        DB::commit();

        return response('success', 201);
    }

    public function import_logs()
    {
        $file = 'import.index';
        $data = [

            'module' => 'import', 
            'option' => 'import', 
            'title' => 'import',
            'icon' => 'mdi mdi-account-storage',
            'file' => 'import.form',
        ];
        return view($file, $data);
    }

    public function create_logs_file(Request $request)
    {  
        $date_from  = $request->get('date_from');
        $date_to    = $request->get('date_to');
        $logs       = $this->get_attendance_logs($date_from, $date_to);
        $tdy        = date('mdY',time());
        $str        = json_encode($logs, JSON_PRETTY_PRINT);
        $myfile     = fopen(public_path('attendance/attendance-logs.json'), "w") or die("Unable to open file!");
        fwrite($myfile, $str);
        fclose($myfile);

        return response('success', 201); 
    }
}
