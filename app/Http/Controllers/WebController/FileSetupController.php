<?php

namespace App\Http\Controllers\WebController;

use App\Http\Controllers\Controller;
use App\Http\Traits\Employee;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

class FileSetupController extends Controller
{
    use Employee;

    private $default_input;

    private $default_rule;

    private $default_attribute;

    private $filesetups;  

    private $filesetup_url;

    private $input_appointment;

    private $input_provider;

    private $input_city;

    private $input_course;

    private $input_salarygrade;

    private $input_plantilla;

    private $input_benefit;

    private $input_adjustment;

    private $input_deduction;

    private $input_loan;

    private $input_bank;

    private $input_absence;

    private $input_leave;

    private $input_office_suspension;

    private $input_work_schedule;

    private $input_philhealth_policy;

    private $input_pagibig_policy;

    private $input_gsis_policy;

    private $input_wage_rate;

    private $input_tax_table;

    private $input_employment_status;

    private $input_training; 

    private $input_signatory;

    private $input_announcement;

    private $input_bank_account;

    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'file_setup';

        $this->default_input = array(
        ['field_name' => 'code', 'input_name' => 'code'],
        ['field_name' => 'name', 'input_name' => 'name']
        );

        $this->default_rule = [
        'code' => 'sometimes|nullable|max:255',
        'name' => 'required|max:255'
        ];

        $this->default_attribute = [
        'code' => strtolower(trans('page.code')),
        'name' => strtolower(trans('page.name'))
        ];

        $this->input_appointment = array(
        ['field_name' => 'appointment_type', 'input_name' => 'appointment_type']
        );

        $this->input_provider = array(
        ['field_name' => 'address', 'input_name' => 'address'],
        ['field_name' => 'training_institution', 'input_name' => 'training_institution'],
        ['field_name' => 'education', 'input_name' => 'education'],
        ['field_name' => 'experience', 'input_name' => 'experience']
        );

        $this->input_city = array(
        ['field_name' => 'province_id', 'input_name' => 'province'],
        ['field_name' => 'zip_code', 'input_name' => 'zip_code']
        );

        $this->input_course = array(
        ['field_name' => 'course_level', 'input_name' => 'course_level']
        );

        $this->input_salarygrade = array(
        ['field_name' => 'step', 'input_name' => 'step'],
        ['field_name' => 'tranche', 'input_name' => 'tranche'],
        ['field_name' => 'amount', 'input_name' => 'amount'],
        );

        $this->input_plantilla = array(
        ['field_name' => 'position_id', 'input_name' => 'position'],
        ['field_name' => 'position_level', 'input_name' => 'position_level'],
        ['field_name' => 'position_classification', 'input_name' => 'position_classification'],
        ['field_name' => 'sector_id', 'input_name' => 'sector'],
        ['field_name' => 'department_id', 'input_name' => 'department'],
        ['field_name' => 'employment_status_id', 'input_name' => 'employment_status_'],
        ['field_name' => 'division_id', 'input_name' => 'division'],
        ['field_name' => 'salary_grade', 'input_name' => 'salary_grade_'],
        ['field_name' => 'salary_step', 'input_name' => 'salary_step'],
        ['field_name' => 'salary_amount', 'input_name' => 'amount'],
        ['field_name' => 'unit_id', 'input_name' => 'unit_'],
        ['field_name' => 'office_id', 'input_name' => 'office_'],
        );

        $this->input_benefit = array(
        ['field_name' => 'tax_type_id', 'input_name' => 'tax_type'],
        ['field_name' => 'computation_type_id', 'input_name' => 'computation_type'],
        ['field_name' => 'amount', 'input_name' => 'amount']
        );

        $this->input_adjustment = array(
        ['field_name' => 'tax_type_id', 'input_name' => 'tax_type'],
        ['field_name' => 'amount', 'input_name' => 'amount']
        );

        $this->input_deduction = array(
        ['field_name' => 'tax_type_id', 'input_name' => 'tax_type'],
        ['field_name' => 'payroll_group_id', 'input_name' => 'payroll_group'],
        ['field_name' => 'amount', 'input_name' => 'amount']
        );

        $this->input_loan = array(
        ['field_name' => 'loan_type_id', 'input_name' => 'loan_type'],
        ['field_name' => 'amount', 'input_name' => 'amount']
        );
     
        $this->input_bank = array(
        ['field_name' => 'branch_name', 'input_name' => 'branch_name'],
        ['field_name' => 'account_no', 'input_name' => 'account_no']
        );

        $this->input_absence = array(
        ['field_name' => 'absence_type', 'input_name' => 'absence_type']
        );

        $this->input_holiday = array(
        ['field_name' => 'start_date', 'input_name' => 'start_date'],
        ['field_name' => 'end_date', 'input_name' => 'end_date'],
        ['field_name' => 'legal_holiday', 'input_name' => 'legal_holiday'],
        ['field_name' => 'yearly', 'input_name' => 'yearly'],
        );

        $this->input_office_suspension = array(
        ['field_name' => 'start_date', 'input_name' => 'start_date'],
        ['field_name' => 'end_date', 'input_name' => 'end_date'],
        ['field_name' => 'start_time', 'input_name' => 'start_time'],
        ['field_name' => 'whole_day', 'input_name' => 'whole_day'],
        ['field_name' => 'time_in_required', 'input_name' => 'time_in_required'],
        ['field_name' => 'office_id', 'input_name' => 'office']
        );

        $this->input_work_schedule = array(
        ['field_name' => 'schedule_type', 'input_name' => 'schedule_type']
        );

        $this->input_employment_status = array(
         ['field_name' => 'category_status', 'input_name' => 'category']
        );

        foreach($this->day as $key => $val)
        {
            $this->input_work_schedule[] = ['field_name' => $val.'_time_in', 'input_name' => $val.'_time_in'];
            $this->input_work_schedule[] = ['field_name' => $val.'_lunch_out', 'input_name' => $val.'_lunch_out'];
            $this->input_work_schedule[] = ['field_name' => $val.'_lunch_in', 'input_name' => $val.'_lunch_in'];
            $this->input_work_schedule[] = ['field_name' => $val.'_time_out', 'input_name' => $val.'_time_out'];
            $this->input_work_schedule[] = ['field_name' => $val.'_strict_mid', 'input_name' => $val.'_strict_mid'];
            $this->input_work_schedule[] = ['field_name' => $val.'_flexi_time', 'input_name' => $val.'_flexi_time'];
            $this->input_work_schedule[] = ['field_name' => $val.'_restday', 'input_name' => $val.'_restday'];
        }

        $this->input_philhealth_policy = array(
        ['field_name' => 'pay_period', 'input_name' => 'pay_period'],
        ['field_name' => 'deduction_period', 'input_name' => 'deduction_period'],
        ['field_name' => 'policy_type', 'input_name' => 'policy_type'],
        ['field_name' => 'based_on', 'input_name' => 'based_on']
        );

        $this->input_pagibig_policy = array(
        ['field_name' => 'pay_period', 'input_name' => 'pay_period'],
        ['field_name' => 'deduction_period', 'input_name' => 'deduction_period'],
        ['field_name' => 'policy_type', 'input_name' => 'policy_type'],
        ['field_name' => 'based_on', 'input_name' => 'based_on']
        );

        $this->input_gsis_policy = array(
        ['field_name' => 'pay_period', 'input_name' => 'pay_period'],
        ['field_name' => 'deduction_period', 'input_name' => 'deduction_period'],
        ['field_name' => 'policy_type', 'input_name' => 'policy_type'],
        ['field_name' => 'based_on', 'input_name' => 'based_on']
        );

        $this->input_wage_rate = array(
        ['field_name' => 'wage_rate', 'input_name' => 'amount'],
        ['field_name' => 'effective_date', 'input_name' => 'effective_date'],
        ['field_name' => 'based_on', 'input_name' => 'based_on']
        );

        $this->input_tax_table = array(
        ['field_name' => 'effective_date', 'input_name' => 'effective_date'],
        );

        foreach($this->tax_table_bracket as $key => $val)
        {
            $this->input_tax_table[] = ['field_name' => $val.'_level_1', 'input_name' => $val.'_level_1'];
            $this->input_tax_table[] = ['field_name' => $val.'_level_2', 'input_name' => $val.'_level_2'];
            $this->input_tax_table[] = ['field_name' => $val.'_level_3', 'input_name' => $val.'_level_3'];
            $this->input_tax_table[] = ['field_name' => $val.'_level_4', 'input_name' => $val.'_level_4'];
            $this->input_tax_table[] = ['field_name' => $val.'_level_5', 'input_name' => $val.'_level_5'];
            $this->input_tax_table[] = ['field_name' => $val.'_level_6', 'input_name' => $val.'_level_6'];
        }

        $this->input_training = array(
        ['field_name' => 'training_type', 'input_name' => 'training_type'],
        );

        $this->input_signatory = array(
         ['field_name' => 'employee_id', 'input_name' => 'name'],
         ['field_name' => 'module', 'input_name' => 'module'],
         ['field_name' => 'position', 'input_name' => 'position'],
         ['field_name' => 'division', 'input_name' => 'division'],
         ['field_name' => 'department', 'input_name' => 'department'],
        );

        $this->input_announcement = array(
        ['field_name' => 'announcement', 'input_name' => 'announcement'],
        ['field_name' => 'start_date', 'input_name' => 'start_date'],
        ['field_name' => 'end_date', 'input_name' => 'end_date'],
        ['field_name' => 'status', 'input_name' => 'status']
        );

        $this->input_bank_account = array(
        ['field_name' => 'bank_id', 'input_name' => 'bank'],
        ['field_name' => 'bank_account', 'input_name' => 'bank_account']
        );

        $this->filesetups = $this->list_filesetups();

        $this->filesetup_url = url('/file_setup');
    }

    public function index()
    {
        return view('file_setup.index', ['module' => $this->module, 'option' => null, 'list_filesetups' => $this->filesetups, 'filesetup_url' => $this->filesetup_url]);
    }

    public function file_setup(request $request, $option)
    {
        try
        {
            $this->is_filesetup_option_exist($option);

            $default_table_id = $option.'_tbl';
            $default_json_url = url('/file_setup/datatables/'.$option);
            $view = 'file_setup.index';

            if(in_array($option, ['appointment_status', 'salary_grade', 'plantilla_items', 'cities', 'benefits', 'adjustments', 'deductions', 'loans', 'banks', 'absences', 'holidays', 'office_suspensions', 'employment_status', 'signatories', 'announcements']))
            {
                if($option == 'appointment_status')
                {
                    $default_columns = array(['data' => 'action', 'sortable' => false],
                    ['data' => 'name'],
                    ['data' => 'appointment_type_lbl'],
                    ['data' => 'remarks']);   
                }
                elseif($option == 'salary_grade')
                {
                    $default_columns = array(['data' => 'action', 'sortable' => false],
                    ['data' => 'name', 'class' => 'text-center'],
                    ['data' => 'step', 'class' => 'text-center'],
                    ['data' => 'tranche', 'class' => 'text-center'],
                    ['data' => 'amount', 'class' => 'text-right']);
                }
                elseif($option == 'plantilla_items')
                {
                    $default_columns = array(['data' => 'action', 'sortable' => false],
                    ['data' => 'name'],
                    ['data' => 'position_name'],
                    ['data' => 'salary_grade'],
                    ['data' => 'salary_step']);
                }
                elseif($option == 'cities')
                {
                    $default_columns = array(['data' => 'action', 'sortable' => false],
                    ['data' => 'name'],
                    ['data' => 'province_name'],
                    ['data' => 'zip_code']);
                }
                elseif($option == 'benefits')
                {
                    $default_columns = array(['data' => 'action', 'sortable' => false],
                    ['data' => 'name'],
                    ['data' => 'tax_type'],
                    ['data' => 'computation_type'],
                    ['data' => 'amount', 'class' => 'text-right'],
                    ['data' => 'remarks'],
                    );
                }
                elseif($option == 'adjustments')
                {
                    $default_columns = array(['data' => 'action', 'sortable' => false],
                    ['data' => 'name'],
                    ['data' => 'tax_type'],
                    ['data' => 'amount', 'class' => 'text-right'],
                    ['data' => 'remarks'],
                    );
                }
                elseif($option == 'deductions')
                {
                    $default_columns = array(['data' => 'action', 'sortable' => false],
                    ['data' => 'name'],
                    ['data' => 'tax_type'],
                    ['data' => 'payroll_group'],
                    ['data' => 'amount', 'class' => 'text-right'],
                    ['data' => 'remarks'],
                    );
                }
                elseif($option == 'loans')
                {
                    $default_columns = array(['data' => 'action', 'sortable' => false],
                    ['data' => 'name'],
                    ['data' => 'loan_type'],
                    ['data' => 'amount', 'class' => 'text-right'],
                    ['data' => 'remarks'],
                    );
                }
                elseif($option == 'banks')
                {
                    $default_columns = array(['data' => 'action', 'sortable' => false],
                    ['data' => 'name'],
                    ['data' => 'branch_name'],
                    ['data' => 'account_no'],
                    ['data' => 'remarks'],
                    );
                }
                elseif($option == 'absences')
                {
                    $default_columns = array(['data' => 'action', 'sortable' => false],
                    ['data' => 'name'],
                    ['data' => 'absence_type'],
                    ['data' => 'remarks'],
                    );
                }
                elseif($option == 'holidays')
                {
                    $default_columns = array(['data' => 'action', 'sortable' => false],
                    ['data' => 'name'],
                    ['data' => 'start_date'],
                    ['data' => 'end_date'],
                    ['data' => 'remarks']
                    );
                }
                elseif($option == 'office_suspensions')
                {
                    $default_columns = array(['data' => 'action', 'sortable' => false],
                    ['data' => 'name'],
                    ['data' => 'start_date'],
                    ['data' => 'end_date'],
                    ['data' => 'remarks']
                    );
                }
                elseif($option == 'employment_status')
                {
                    $default_columns = array(['data' => 'action', 'sortable' => false],
                    ['data' => 'name'],
                    ['data' => 'category_name'],
                    ['data' => 'remarks']
                    );
                }
                elseif($option == 'signatories')
                {
                    $default_columns = array(['data' => 'action', 'sortable' => false],
                    ['data' => 'full_name'],
                    ['data' => 'position'],
                    ['data' => 'division'],
                    ['data' => 'department']    
                    );
                }
                elseif($option == 'announcements')
                {
                    $default_columns = array(['data' => 'action', 'sortable' => false],
                    ['data' => 'announcement'],
                    ['data' => 'start_date'],
                    ['data' => 'end_date']
                    );
                }

                $file = 'file_setup.'.$option.'.table';
            }
            else
            {
                $default_columns = array(['data' => 'action', 'sortable' => false],
                ['data' => 'name'],
                ['data' => 'remarks']);

                $file = 'file_setup.default.table';
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }
        
        return view($view, ['module' => $this->module, 'option' => $option, 'default_table_id' => $default_table_id, 'default_columns' => $default_columns, 'default_json_url' => $default_json_url, 'list_filesetups' => $this->filesetups, 'filesetup_url' => $this->filesetup_url, 'file' => $file]);
    }

    public function create_file_setup(request $request, $option)
    {
        try
        {
            $view = 'file_setup.create';

            $this->is_filesetup_option_exist($option);

            $data = ['module' => $this->module, 'option' => $option, 'list_filesetups' => $this->filesetups, 'frm_action' => url('/file_setup/'.$option.'/store')];

            if(in_array($option, ['appointment_status', 'salary_grade', 'plantilla_items', 'cities', 'benefits', 'adjustments', 'deductions', 'loans', 'banks', 'absences', 'holidays', 'office_suspensions', 'work_schedules', 'wage_rate', 'tax_table', 'philhealth_policy', 'pagibig_policy', 'gsis_policy', 'employment_status', 'trainings', 'signatories', 'announcements', 'bank_accounts']))
            {
                if($option == 'salary_grade')
                {   
                    $data['salary_grade'] = $this->salary_grade;

                    $data['tranche'] = $this->tranche;

                    $data['step'] = $this->step;

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_salarygrade);
                }
                elseif($option == 'plantilla_items')
                {
                    $data['positions'] = $this->list_positions();  

                    $data['salary_grade'] = $this->salary_grade;

                    $data['step'] = $this->step;

                    $data['divisions'] = $this->list_divisions();

                    $data['sectors'] = $this->list_sectors();

                    $data['departments'] = $this->list_departments();

                    $data['employment_status'] = $this->list_employment_status();

                    $data['offices'] = $this->list_offices();

                    $data['units'] = $this->list_units();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_plantilla);
                }
                elseif($option == 'appointment_status')
                {
                    $data['appointment_type'] = $this->list_appointment_type();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_appointment);
                }
                elseif($option == 'providers')
                {
                    $data['default_inputs'] = array_merge($this->default_input, $this->input_provider);
                }
                elseif($option == 'cities')
                {
                    $data['provinces'] = $this->list_provinces();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_city);
                }
                elseif($option == 'courses')
                {
                    $data['default_inputs'] = array_merge($this->default_input, $this->input_course);
                }
                elseif($option == 'benefits')
                {
                    $data['tax_types'] = $this->list_taxtypes();
                    $data['computation_types'] = $this->list_computationtypes();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_benefit);
                }
                elseif($option == 'adjustments')
                {
                    $data['tax_types'] = $this->list_taxtypes();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_adjustment);
                }
                elseif($option == 'deductions')
                {
                    $data['tax_types'] = $this->list_taxtypes();
                    $data['payroll_groups'] = $this->list_payrollgroups();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_deduction);
                }
                elseif($option == 'loans')
                {
                    $data['loan_types'] = $this->list_loantypes();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_loan);
                }
                elseif($option == 'banks')
                {
                    $data['default_inputs'] = array_merge($this->default_input, $this->input_bank);
                }
                elseif($option == 'absences')
                {
                    $data['default_inputs'] = array_merge($this->default_input, $this->input_absence);
                }
                elseif($option == 'holidays')
                {
                    $data['default_inputs'] = array_merge($this->default_input, $this->input_holiday);
                }
                elseif($option == 'office_suspensions')
                {
                    $data['offices'] = $this->list_offices();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_office_suspension);
                }
                elseif($option == 'work_schedules')
                {
                    $data['schedule_types'] = $this->list_scheduletypes();

                    $data['days'] = $this->day;

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_work_schedule);
                }
                elseif($option == 'wage_rate')
                {
                    $data['based_on'] = $this->based_on;

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_wage_rate);
                }
                elseif($option == 'tax_table')
                {
                    $data['default_inputs'] = array_merge($this->default_input, $this->input_tax_table);
                }
                elseif(in_array($option, ['philhealth_policy', 'pagibig_policy', 'gsis_policy']))
                {
                    $data['pay_period'] = $this->pay_period;

                    $data['deduction_period'] = $this->deduction_period;

                    $data['policy_type'] = $this->policy_type;

                    $data['based_on'] = $this->based_on;

                    if($option == 'philhealth_policy')
                    {
                        $data['default_inputs'] = array_merge($this->default_input, $this->input_philhealth_policy);
                    }
                    elseif($option == 'pagibig_policy')
                    {
                        $data['default_inputs'] = array_merge($this->default_input, $this->input_pagibig_policy);
                    }
                    elseif($option == 'gsis_policy')
                    {
                        $data['default_inputs'] = array_merge($this->default_input, $this->input_gsis_policy);
                    }
                }
                elseif($option == 'employment_status')
                {
                   $data['default_inputs'] = array_merge($this->default_input, $this->input_employment_status);
                }
                elseif($option == 'trainings')
                {
                    $data['training_types'] = $this->list_training_types();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_training);
                }
                elseif($option == 'signatories')
                {
                    $data['employees'] = $this->list_employees();
                    $data['list_positions'] = $this->list_positions();
                    $data['list_divisions'] = $this->list_divisions();
                    $data['list_departments'] = $this->list_departments();
                    $data['signatory_pages'] = $this->signatory_pages;
                    $data['default_inputs'] = $this->input_signatory;
                }
                elseif($option == 'announcements')
                {
                    $data['default_inputs'] = $this->input_announcement;
                }
                elseif($option == 'bank_accounts')
                {
                    $data['default_inputs'] = array_merge($this->default_input, $this->input_bank_account);

                    $data['banks'] = $this->list_banks();
                }

                $data['file'] = 'file_setup.'.$option.'.add_form';
            }
            else
            {
                $data['default_inputs'] = $this->default_input;

                $data['file'] = 'file_setup.default.default_form';
            }       
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }

        return view($view, $data);
    }

    public function store_file_setup(request $request, $option)
    {
        try 
        {  
            $rules = $this->default_rule;
            $attribute_name = $this->default_attribute;

            $insert_data = [
            'code' => $request->get('code'),
            'name' => $request->get('name'),
            'created_by' => Auth::user()->id,
            'created_at' => DB::raw('now()')
            ];

            $this->is_filesetup_option_exist($option);

            if($option == 'plantilla_items')
            {
                $insert_data = array_merge($insert_data, [
                'position_id' => $request->get('position'),
                'division_id' => $request->get('division'),
                'position_level' => $request->get('position_level'),
                'position_classification' => $request->get('position_classification'),
                'salary_grade' => $request->get('salary_grade_'),
                'salary_step' => $request->get('salary_step'),
                'sector_id' => $request->get('sector'),
                'department_id' => $request->get('department'),
                'employment_status_id' => $request->get('employment_status_'),
                'unit_id' => $request->get('unit_'),
                'office_id' => $request->get('office_')
                ]);

                $rules = array_merge($rules, [
                'position' => 'required|in:'.implode(',', $this->get_value_by($this->list_positions(), 'id')),
                'position_level' => 'sometimes|nullable|max:255',
                'position_classification' => 'sometimes|nullable|max:255',
                'sector' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_sectors(), 'id')),
                'department' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_departments(), 'id')),
                'employment_status_' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_employment_status(), 'id')),
                'division' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_divisions(), 'id')),
                'salary_grade_' => 'required|in:'.implode(',', $this->salary_grade),
                'salary_step' => 'required|in:'.implode(',', $this->step)
                ]);
            }
            elseif($option == 'appointment_status')
            {
                $insert_data = array_merge($insert_data, [
                'appointment_type' => $request->get('appointment_type')
                ]);

                $rules = array_merge($rules, [
                'appointment_type' => 'required|in:'.implode(',', $this->get_value_by($this->list_appointment_type(), 'id'))
                ]);
            }
            elseif($option == 'providers')
            {
                $insert_data = array_merge($insert_data, [
                'address' => $request->get('address'),
                'training_institution' => $request->get('training_institution'),
                'education' => $request->get('education'),
                'experience' => $request->get('experience')
                ]);

                $rules = array_merge($rules, [
                'address' => 'sometimes|nullable|max:255',
                'training_institution' => 'sometimes|nullable|max:255',
                'education' => 'sometimes|nullable|max:255',
                'experience' => 'sometimes|nullable|max:255',
                'expertise' => 'sometimes|nullable|max:255',
                ]);
            }
            elseif($option == 'cities')
            {
                $insert_data = array_merge($insert_data, [
                'province_id' => $request->get('province'),
                'zip_code' => $request->get('zip_code')
                ]);

                $rules = array_merge($rules, [
                'province' => 'required|in:'.implode(',', $this->get_value_by($this->list_provinces(), 'id')),
                'zip_code' => 'required|max:50'
                ]);
            }
            elseif($option == 'courses')
            {
                $insert_data = array_merge($insert_data, [
                'course_level' => $request->get('course_level')
                ]);

                $rules = array_merge($rules, [
                'course_level' => 'in:3,4,5'  
                ]);
            }
            elseif($option == 'salary_grade')
            {   
                $insert_data = array_merge($insert_data, [
                'tranche' => $request->get('tranche'),
                'step' => $request->get('step'),
                'amount' => $request->get('amount')
                ]);

                $rules = [
                'code' => 'sometimes|nullable|max:45',
                'name' => 'required|in:'.implode(',', $this->salary_grade),
                'tranche' => 'required|in:'.implode(',', $this->tranche),
                'step' => 'required|in:'.implode(',', $this->step),
                'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/'
                ];
            }
            elseif($option == 'benefits')
            {
                $insert_data = array_merge($insert_data, [
                'tax_type_id' => $request->get('tax_type'),
                'computation_type_id' => $request->get('computation_type'),
                'amount' => $request->get('amount'),
                ]);

                $rules = array_merge($rules, [
                'tax_type' => 'required|in:'.implode(',', $this->get_value_by($this->list_taxtypes(), 'id')),
                'computation_type' => 'required|in:'.implode(',', $this->get_value_by($this->list_computationtypes(), 'id')),
                'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/'
                ]);
            }
            elseif($option == 'adjustments')
            {
                $insert_data = array_merge($insert_data, [
                'tax_type_id' => $request->get('tax_type'),
                'amount' => $request->get('amount'),
                ]);

                $rules = array_merge($rules, [
                'tax_type' => 'required|in:'.implode(',', $this->get_value_by($this->list_taxtypes(), 'id')),
                'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/'
                ]);
            }
            elseif($option == 'deductions')
            {
                $insert_data = array_merge($insert_data, [
                'tax_type_id' => $request->get('tax_type'),
                'payroll_group_id' => $request->get('payroll_group'),
                'amount' => $request->get('amount'),
                ]);

                $rules = array_merge($rules, [
                'tax_type' => 'required|in:'.implode(',', $this->get_value_by($this->list_taxtypes(), 'id')),
                'payroll_group' => 'required|in:'.implode(',', $this->get_value_by($this->list_payrollgroups(), 'id')),
                'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/'
                ]);
            }
            elseif($option == 'loans')
            {
                $insert_data = array_merge($insert_data, [
                'loan_type_id' => $request->get('loan_type'),
                'amount' => $request->get('amount'),
                ]);

                $rules = array_merge($rules, [
                'loan_type' => 'required|in:'.implode(',', $this->get_value_by($this->list_loantypes(), 'id')),
                'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/'
                ]);
            }
            elseif($option == 'banks')
            {
                $insert_data = array_merge($insert_data, [
                'branch_name' => $request->get('branch_name'),
                'account_no' => $request->get('account_no'),
                ]);

                $rules = array_merge($rules, [
                'branch_name' => 'required|max:255',
                'account_no' => 'required|max:45',
                ]);
            }
            elseif($option == 'absences')
            {
                $insert_data = array_merge($insert_data, [
                'absence_type' => $request->get('absence_type')
                ]);

                $rules = array_merge($rules, [
                'absence_type' => 'required|in:NC,IN'
                ]);
            }
            elseif($option == 'holidays')
            {
                $insert_data = array_merge($insert_data, [
                'start_date' => $request->get('start_date'),
                'end_date' => $request->get('end_date'),
                'legal_holiday' => $request->get('legal_holiday'),
                'yearly' => $request->get('yearly')
                ]);

                $rules = array_merge($rules, [
                'start_date' => 'required|date_format:Y-m-d',
                'end_date' => 'required|date_format:Y-m-d',
                'legal_holiday' => 'required|in:0,1',
                'yearly' => 'required|in:0,1',
                ]);
            }
            elseif($option == 'office_suspensions')
            {
                $insert_data = array_merge($insert_data, [
                'whole_day' => $request->get('whole_day'),
                'time_in_required' => $request->get('time_in_required'),
                'office_id' => $request->get('office')
                ]);

                $rules = array_merge($rules, [
                'start_date' => 'required|date_format:Y-m-d',
                'end_date' => 'required|date_format:Y-m-d',
                'start_time' => 'required|date_format:h:i A',
                'whole_day' => 'required|in:0,1',
                'time_in_required' => 'required|in:0,1',
                'office' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_offices(), 'id'))
                ]);
            }
            elseif($option == 'work_schedules')
            {
                $insert_data = array_merge($insert_data, [
                'schedule_type' => $request->get('schedule_type')
                ]);

                $rules = array_merge($rules, [
                'schedule_type' => 'required|in:'.implode(',', $this->get_value_by($this->list_scheduletypes(), 'id'))
                ]);

                foreach($this->day as $key => $val)
                {
                    $rules[$val.'_time_in'] = 'sometimes|nullable|date_format:h:i A';
                    $rules[$val.'_lunch_out'] =  'sometimes|nullable|date_format:h:i A';
                    $rules[$val.'_lunch_in'] = 'sometimes|nullable|date_format:h:i A';
                    $rules[$val.'_time_out'] = 'sometimes|nullable|date_format:h:i A';
                    $rules[$val.'_strict_mid'] = 'required|in:0,1';
                    $rules[$val.'_flexi_time'] = 'sometimes|nullable|date_format:h:i A';
                    $rules[$val.'_restday'] = 'sometimes|nullable|in:0,1';
                }
            }
            elseif($option == 'wage_rate')
            {
                $insert_data = array_merge($insert_data, [
                'effective_date' => $request->get('effective_date'),
                'wage_rate' => $request->get('amount'),
                'based_on' => $request->get('based_on')
                ]);

                $rules = array_merge($rules, [
                'effective_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'based_on' => 'required|in:'.implode(',', $this->based_on)
                ]);
            }
            elseif($option == 'tax_table')
            {
                $insert_data = array_merge($insert_data, [
                'effective_date' => $request->get('effective_date')
                ]);

                $rules = array_merge($rules, [
                'effective_date' => 'required|date_format:Y-m-d'
                ]);

                foreach($this->tax_table_bracket as $key => $val)
                {
                    $rules[$val.'_level_1'] = 'sometimes|nullable|regex:/^\d+(\.\d{1,2})?$/';
                    $rules[$val.'_level_2'] = 'sometimes|nullable|regex:/^\d+(\.\d{1,2})?$/';
                    $rules[$val.'_level_3'] = 'sometimes|nullable|regex:/^\d+(\.\d{1,2})?$/';
                    $rules[$val.'_level_4'] = 'sometimes|nullable|regex:/^\d+(\.\d{1,2})?$/';
                    $rules[$val.'_level_5'] = 'sometimes|nullable|regex:/^\d+(\.\d{1,2})?$/';
                    $rules[$val.'_level_6'] = 'sometimes|nullable|regex:/^\d+(\.\d{1,2})?$/';
                }
            }
            elseif(in_array($option, ['philhealth_policy', 'pagibig_policy', 'gsis_policy']))
            {
                $insert_data = array_merge($insert_data, [
                'pay_period' => $request->get('pay_period'),
                'deduction_period' => $request->get('deduction_period'),
                'policy_type' => $request->get('policy_type'),
                'based_on' => $request->get('based_on')
                ]);

                $rules = array_merge($rules, [
                'pay_period' => 'required|in:'.implode(',', $this->pay_period),
                'deduction_period' => 'required|in:'.implode(',', $this->deduction_period),
                'policy_type' => 'required|in:'.implode(',', $this->policy_type),
                'based_on' => 'required|in:'.implode(',', $this->based_on)
                ]);
            }
            elseif($option == 'employment_status')
            {
                $insert_data = array_merge($insert_data, [
                'category_status' => $request->get('category')
                ]);

                $rules = array_merge($rules, [
                'category' => 'required|in:'.implode(',', array_keys(config('params.plantilla_category')))
                ]);
            }
            elseif($option == 'trainings')
            {
                $insert_data = array_merge($insert_data, [
                'training_type' => $request->get('training_type')
                ]);

                $rules = array_merge($rules, [
                'training_type' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_training_types(), 'id'))
                ]);
            }
            elseif($option == 'signatories')
            {
                $insert_data = [
                'employee_id' => $request->get('name'),
                'module' => $request->get('module'),
                'position' => $request->get('position'),
                'division' => $request->get('division'),
                'department' => $request->get('department')
                ];

                $rules = array_merge($rules, [
                'name' => 'required|in:'.implode(',', $this->get_value_by($this->list_employees(), 'id')),
                'module' => 'sometimes|nullable|in:'.implode(',', array_keys($this->signatory_pages)),
                'position' => 'sometimes|nullable|max:255',
                'division' => 'sometimes|nullable|max:255',
                'department' => 'sometimes|nullable|max:255',
                ]);
            }
            elseif($option == 'announcements')
            {
                $insert_data = [
                'announcement' => $request->get('announcement'),
                'start_date' => $request->get('start_date'),
                'end_date' => $request->get('end_date'),
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ];

                $rules = [
                'announcement' => 'required',
                'start_date' => 'required',
                'end_date' => 'required'
                ];
            }
            elseif($option == 'bank_accounts')
            {
                $insert_data = array_merge($insert_data, [
                'bank_id' => $request->get('bank'),
                'bank_account' => $request->get('bank_account')
                ]);

                $rules = array_merge($rules, [
                'bank' => 'required|in:'.implode(',', $this->get_value_by($this->list_banks(), 'id')),
                'bank_account' => 'required|max:255',
                ]);
            }
            
            $this->validate_request($request->all(), $rules, $attribute_name);

            if($option == 'plantilla_items')
            {
                $data = $this->check_exist_salary_grade($request->get('salary_grade_'), $request->get('salary_step'));
                $insert_data['salary_amount'] = $data->amount; 
            }
            elseif($option == 'office_suspensions')
            {
                $insert_data['start_date'] = $request->get('start_date');   
                $insert_data['end_date'] = $request->get('end_date');
                $hr_24 = date("H:i", strtotime($request->get('start_time')));
                $timesplit = explode(':', $hr_24);
                $insert_data['start_time'] = ($timesplit[0]*60) + ($timesplit[1]);
            }
            elseif($option == 'work_schedules')
            {
                foreach($this->day as $key => $val)
                {
                    $time_in = $this->time_to_minutes($request->get($val.'_time_in'));
                    $insert_data[$val.'_time_in'] = $time_in == 0 ? null : $time_in;

                    $lunch_out = $this->time_to_minutes($request->get($val.'_lunch_out'));
                    $insert_data[$val.'_lunch_out'] = $lunch_out == 0 ? null : $lunch_out;

                    $lunch_in = $this->time_to_minutes($request->get($val.'_lunch_in'));
                    $insert_data[$val.'_lunch_in'] = $lunch_in == 0 ? null : $lunch_in;

                    $time_out = $this->time_to_minutes($request->get($val.'_time_out'));
                    $insert_data[$val.'_time_out'] = $time_out == 0 ? null : $time_out;

                    $insert_data[$val.'_strict_mid'] = $request->get($val.'_strict_mid');

                    $flexi_time = $this->time_to_minutes($request->get($val.'_flexi_time'));
                    $insert_data[$val.'_flexi_time'] = $flexi_time == 0 ? null : $flexi_time;

                    $insert_data[$val.'_restday'] = $request->get($val.'_restday');
                }
            }
            elseif($option == 'philhealth_policy')
            {
                $insert_data['below'] = $this->philhealth_below;
                $insert_data['above'] = $this->philhealth_above;
            }
            elseif($option == 'gsis_policy')
            {
                $insert_data['ee_percentage'] = $this->default_ee_percentage;
                $insert_data['er_percentage'] = $this->default_er_percentage;
                $insert_data['ec_percentage'] = $this->default_ec_percentage;
            }
            elseif($option == 'tax_table')
            {
                foreach($this->tax_table_bracket as $key => $val)
                {
                    $bracket = array();

                    $bracket[] = $request->get($val.'_level_1') ? $request->get($val.'_level_1') : 0;
                    $bracket[] = $request->get($val.'_level_2') ? $request->get($val.'_level_2') : 0;
                    $bracket[] = $request->get($val.'_level_3') ? $request->get($val.'_level_3') : 0;
                    $bracket[] = $request->get($val.'_level_4') ? $request->get($val.'_level_4') : 0;
                    $bracket[] = $request->get($val.'_level_5') ? $request->get($val.'_level_5') : 0;
                    $bracket[] = $request->get($val.'_level_6') ? $request->get($val.'_level_6') : 0;

                    $insert_data[$val.'_bracket'] = implode(',', $bracket);
                }
            }

            DB::beginTransaction();

            DB::table($option)
            ->insert($insert_data);

            DB::commit();
        } 
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }

        return response('success', 201);
    }
    
    public function edit_file_setup(request $request, $option, $id)
    {
        try
        {
            $view = 'file_setup.edit';

            $this->is_filesetup_option_exist($option);

            $data = ['module' => $this->module, 'option' => $option, 'list_filesetups' => $this->filesetups, 'id' => $id, 'frm_action' => url('/file_setup/'.$option.'/update')];

            if(in_array($option, ['appointment_status', 'salary_grade', 'plantilla_items', 'cities', 'benefits', 'adjustments', 'deductions', 'loans', 'banks', 'absences', 'holidays', 'office_suspensions', 'work_schedules', 'wage_rate', 'tax_table', 'philhealth_policy', 'pagibig_policy', 'gsis_policy', 'employment_status', 'trainings', 'signatories', 'announcements', 'bank_accounts']))
            {
                if($option == 'salary_grade')
                {   
                    $data['salary_grade'] = $this->salary_grade;

                    $data['tranche'] = $this->tranche;

                    $data['step'] = $this->step;

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_salarygrade);
                }
                elseif($option == 'plantilla_items')
                {
                    $data['positions'] = $this->list_positions();  

                    $data['salary_grade'] = $this->salary_grade;

                    $data['step'] = $this->step;

                    $data['sectors'] = $this->list_sectors();

                    $data['departments'] = $this->list_departments();

                    $data['employment_status'] = $this->list_employment_status();

                    $data['divisions'] = $this->list_divisions();

                    $data['units'] = $this->list_units();

                    $data['offices'] = $this->list_offices();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_plantilla);
                }
                elseif($option == 'appointment_status')
                {
                    $data['appointment_type'] = $this->list_appointment_type();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_appointment);
                }
                elseif($option == 'providers')
                {
                    $data['default_inputs'] = array_merge($this->default_input, $this->input_provider);
                }
                elseif($option == 'cities')
                {
                    $data['provinces'] = $this->list_provinces();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_city);
                }
                elseif($option == 'courses')
                {
                    $data['default_inputs'] = array_merge($this->default_input, $this->input_course);
                }
                elseif($option == 'benefits')
                {
                    $data['tax_types'] = $this->list_taxtypes();
                    $data['computation_types'] = $this->list_computationtypes();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_benefit);
                }
                elseif($option == 'adjustments')
                {
                    $data['tax_types'] = $this->list_taxtypes();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_adjustment);
                }
                elseif($option == 'deductions')
                {
                    $data['tax_types'] = $this->list_taxtypes();
                    $data['payroll_groups'] = $this->list_payrollgroups();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_deduction);
                }
                elseif($option == 'loans')
                {
                    $data['loan_types'] = $this->list_loantypes();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_loan);
                }
                elseif($option == 'banks')
                {
                    $data['default_inputs'] = array_merge($this->default_input, $this->input_bank);
                }
                elseif($option == 'absences')
                {
                    $data['default_inputs'] = array_merge($this->default_input, $this->input_absence);
                }
                elseif($option == 'holidays')
                {
                    $data['default_inputs'] = array_merge($this->default_input, $this->input_holiday);
                }
                elseif($option == 'office_suspensions')
                {
                    $data['offices'] = $this->list_offices();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_office_suspension);
                }
                elseif($option == 'work_schedules')
                {
                    $data['schedule_types'] = $this->list_scheduletypes();

                    $data['days'] = $this->day;

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_work_schedule);
                }
                elseif($option == 'wage_rate')
                {
                    $data['based_on'] = $this->based_on;

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_wage_rate);
                }
                elseif(in_array($option, ['philhealth_policy', 'pagibig_policy', 'gsis_policy']))
                {
                    $data['pay_period'] = $this->pay_period;

                    $data['deduction_period'] = $this->deduction_period;

                    $data['policy_type'] = $this->policy_type;

                    $data['based_on'] = $this->based_on;

                    if($option == 'philhealth_policy')
                    {
                        $data['default_inputs'] = array_merge($this->default_input, $this->input_philhealth_policy);
                    }
                    elseif($option == 'pagibig_policy')
                    {
                        $data['default_inputs'] = array_merge($this->default_input, $this->input_pagibig_policy);
                    }
                    elseif($option == 'gsis_policy')
                    {
                        $data['default_inputs'] = array_merge($this->default_input, $this->input_gsis_policy);
                    }
                }
                elseif($option == 'employment_status')
                {
                   $data['default_inputs'] = array_merge($this->default_input, $this->input_employment_status);
                }
                elseif($option == 'trainings')
                {
                    $data['training_types'] = $this->list_training_types();

                    $data['default_inputs'] = array_merge($this->default_input, $this->input_training);
                }
                elseif($option == 'signatories')
                {
                    $data['employees'] = $this->list_employees();
                    $data['signatory_pages'] = $this->signatory_pages;

                    $data['list_positions'] = $this->list_positions();
                    $data['list_divisions'] = $this->list_divisions();
                    $data['list_departments'] = $this->list_departments();

                    $data['default_inputs'] = $this->input_signatory;
                }
                elseif($option == 'announcements')
                {
                    $data['default_inputs'] = $this->input_announcement;
                }
                elseif($option == 'bank_accounts')
                {
                    $data['default_inputs'] = array_merge($this->default_input, $this->input_bank_account);

                    $data['banks'] = $this->list_banks();
                }

                $data['file'] = 'file_setup.'.$option.'.edit_form';
            }
            else
            {
                $data['default_inputs'] = $this->default_input;

                $data['file'] = 'file_setup.default.default_form';
            } 
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }

        return view($view, $data);
    }

    public function update_file_setup(request $request, $option)
    {
        try
        {
            $rules = $this->default_rule;
            $attribute_name = $this->default_attribute;

            $update_data = [
            'code' => $request->get('code'),
            'name' => $request->get('name'),
            'updated_by' => Auth::user()->id,
            'updated_at' => DB::raw('now()')
            ];

            $this->is_filesetup_option_exist($option);

            $this->check_exist_table_record($option, $request->get('id'));

            if($option == 'plantilla_items')
            {
                $update_data = array_merge($update_data, [
                'position_id' => $request->get('position'),
                'division_id' => $request->get('division'),
                'position_level' => $request->get('position_level'),
                'position_classification' => $request->get('position_classification'),
                'salary_grade' => $request->get('salary_grade_'),
                'salary_step' => $request->get('salary_step'),
                'sector_id' => $request->get('sector'),
                'department_id' => $request->get('department'),
                'employment_status_id' => $request->get('employment_status_'),
                'unit_id' => $request->get('unit_'),
                'office_id' => $request->get('office_')
                ]);

                $rules = array_merge($rules, [
                'position' => 'required|in:'.implode(',', $this->get_value_by($this->list_positions(), 'id')),
                'position_level' => 'sometimes|nullable|max:255',
                'position_classification' => 'sometimes|nullable|max:255',
                'sector' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_sectors(), 'id')),
                'department' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_departments(), 'id')),
                'employment_status_' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_employment_status(), 'id')),
                'division' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_divisions(), 'id')),
                'salary_grade_' => 'required|in:'.implode(',', $this->salary_grade),
                'salary_step' => 'required|in:'.implode(',', $this->step)
                ]);
            }
            elseif($option == 'appointment_status')
            {
                $update_data = array_merge($update_data, [
                'appointment_type' => $request->get('appointment_type')
                ]);

                $rules = array_merge($rules, [
                'appointment_type' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_appointment_type(), 'id'))
                ]);
            }
            elseif($option == 'providers')
            {
                $update_data = array_merge($update_data, [
                'address' => $request->get('address'),
                'training_institution' => $request->get('training_institution'),
                'education' => $request->get('education'),
                'experience' => $request->get('experience')
                ]);

                $rules = array_merge($rules, [
                'address' => 'sometimes|nullable|max:255',
                'training_institution' => 'sometimes|nullable|max:255',
                'education' => 'sometimes|nullable|max:255',
                'experience' => 'sometimes|nullable|max:255',
                'expertise' => 'sometimes|nullable|max:255',
                ]);
            }
            elseif($option == 'cities')
            {
                $update_data = array_merge($update_data, [
                'province_id' => $request->get('province'),
                'zip_code' => $request->get('zip_code')
                ]);

                $rules = array_merge($rules, [
                'province' => 'required|in:'.implode(',', $this->get_value_by($this->list_provinces(), 'id')),
                'zip_code' => 'required|max:50'
                ]);
            }
            elseif($option == 'courses')
            {
                $update_data = array_merge($update_data, [
                'course_level' => $request->get('course_level')
                ]);

                $rules = array_merge($rules, [
                'course_level' => 'in:3,4,5'  
                ]);
            }
            elseif($option == 'salary_grade')
            {   
                $update_data = array_merge($update_data, [
                'tranche' => $request->get('tranche'),
                'step' => $request->get('step'),
                'amount' => $request->get('amount')
                ]);

                $rules = [
                'code' => 'sometimes|nullable|max:45',
                'name' => 'required|in:'.implode(',', $this->salary_grade),
                'tranche' => 'required|in:'.implode(',', $this->tranche),
                'step' => 'required|in:'.implode(',', $this->step),
                'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/'
                ];
            }
            elseif($option == 'benefits')
            {
                $update_data = array_merge($update_data, [
                'tax_type_id' => $request->get('tax_type'),
                'computation_type_id' => $request->get('computation_type'),
                'amount' => $request->get('amount'),
                ]);

                $rules = array_merge($rules, [
                'tax_type' => 'required|in:'.implode(',', $this->get_value_by($this->list_taxtypes(), 'id')),
                'computation_type' => 'required|in:'.implode(',', $this->get_value_by($this->list_computationtypes(), 'id')),
                'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/'
                ]);
            }
            elseif($option == 'adjustments')
            {
                $update_data = array_merge($update_data, [
                'tax_type_id' => $request->get('tax_type'),
                'amount' => $request->get('amount'),
                ]);

                $rules = array_merge($rules, [
                'tax_type' => 'required|in:'.implode(',', $this->get_value_by($this->list_taxtypes(), 'id')),
                'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/'
                ]);
            }
            elseif($option == 'deductions')
            {
                $update_data = array_merge($update_data, [
                'tax_type_id' => $request->get('tax_type'),
                'payroll_group_id' => $request->get('payroll_group'),
                'amount' => $request->get('amount'),
                ]);

                $rules = array_merge($rules, [
                'tax_type' => 'required|in:'.implode(',', $this->get_value_by($this->list_taxtypes(), 'id')),
                'payroll_group' => 'required|in:'.implode(',', $this->get_value_by($this->list_payrollgroups(), 'id')),
                'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/'
                ]);
            }
            elseif($option == 'loans')
            {
                $update_data = array_merge($update_data, [
                'loan_type_id' => $request->get('loan_type'),
                'amount' => $request->get('amount'),
                ]);

                $rules = array_merge($rules, [
                'loan_type' => 'required|in:'.implode(',', $this->get_value_by($this->list_loantypes(), 'id')),
                'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/'
                ]);
            }
            elseif($option == 'banks')
            {
                $update_data = array_merge($update_data, [
                'branch_name' => $request->get('branch_name'),
                'account_no' => $request->get('account_no'),
                ]);

                $rules = array_merge($rules, [
                'branch_name' => 'required|max:255',
                'account_no' => 'required|max:45',
                ]);
            }
            elseif($option == 'absences')
            {
                $update_data = array_merge($update_data, [
                'absence_type' => $request->get('absence_type')
                ]);

                $rules = array_merge($rules, [
                'absence_type' => 'required|in:NC,IN'
                ]);
            }
            elseif($option == 'holidays')
            {
                $update_data = array_merge($update_data, [
                'start_date' => $request->get('start_date'),
                'end_date' => $request->get('end_date'),
                'legal_holiday' => $request->get('legal_holiday'),
                'yearly' => $request->get('yearly')
                ]);

                $rules = array_merge($rules, [
                'start_date' => 'required',
                'end_date' => 'required',
                'legal_holiday' => 'required|in:0,1',
                'yearly' => 'required|in:0,1',
                ]);
            }
            elseif($option == 'office_suspensions')
            {
                $update_data = array_merge($update_data, [
                'whole_day' => $request->get('whole_day'),
                'time_in_required' => $request->get('time_in_required'),
                'office_id' => $request->get('office')
                ]);

                $rules = array_merge($rules, [
                'start_date' => 'required|date_format:Y-m-d',
                'end_date' => 'required|date_format:Y-m-d',
                'start_time' => 'required|date_format:h:i A',
                'whole_day' => 'required|in:0,1',
                'time_in_required' => 'required|in:0,1',
                'office' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_offices(), 'id'))
                ]);
            }
            elseif($option == 'work_schedules')
            {
                $update_data = array_merge($update_data, [
                'schedule_type' => $request->get('schedule_type')
                ]);

                $rules = array_merge($rules, [
                'schedule_type' => 'required|in:'.implode(',', $this->get_value_by($this->list_scheduletypes(), 'id'))
                ]);

                foreach($this->day as $key => $val)
                {
                    $rules[$val.'_time_in'] = 'sometimes|nullable|date_format:h:i A';
                    $rules[$val.'_lunch_out'] =  'sometimes|nullable|date_format:h:i A';
                    $rules[$val.'_lunch_in'] = 'sometimes|nullable|date_format:h:i A';
                    $rules[$val.'_time_out'] = 'sometimes|nullable|date_format:h:i A';
                    $rules[$val.'_strict_mid'] = 'required|in:0,1';
                    $rules[$val.'_flexi_time'] = 'sometimes|nullable|date_format:h:i A';
                    $rules[$val.'_restday'] = 'sometimes|nullable|in:0,1';
                }
            }
            elseif($option == 'wage_rate')
            {
                $update_data = array_merge($update_data, [
                'effective_date' => $request->get('effective_date'),
                'wage_rate' => $request->get('amount'),
                'based_on' => $request->get('based_on')
                ]);

                $rules = array_merge($rules, [
                'effective_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'based_on' => 'required|in:'.implode(',', $this->based_on)
                ]);
            }
            elseif(in_array($option, ['philhealth_policy', 'pagibig_policy', 'gsis_policy']))
            {
                $update_data = array_merge($update_data, [
                'pay_period' => $request->get('pay_period'),
                'deduction_period' => $request->get('deduction_period'),
                'policy_type' => $request->get('policy_type'),
                'based_on' => $request->get('based_on')
                ]);

                $rules = array_merge($rules, [
                'pay_period' => 'required|in:'.implode(',', $this->pay_period),
                'deduction_period' => 'required|in:'.implode(',', $this->deduction_period),
                'policy_type' => 'required|in:'.implode(',', $this->policy_type),
                'based_on' => 'required|in:'.implode(',', $this->based_on)
                ]);
            }
            elseif($option == 'employment_status')
            {
                $update_data = array_merge($update_data, [
                'category_status' => $request->get('category')
                ]);

                $rules = array_merge($rules, [
                'category' => 'required|in:'.implode(',', array_keys(config('params.plantilla_category')))
                ]);
            }
            elseif($option == 'signatories')
            {
                $update_data = [
                'employee_id' => $request->get('name'),
                'module' => $request->get('module'),
                'position' => $request->get('position'),
                'division' => $request->get('division'),
                'department' => $request->get('department')
                ];

                $rules = array_merge($rules, [
                'name' => 'required|in:'.implode(',', $this->get_value_by($this->list_employees(), 'id')),
                'module' => 'sometimes|nullable|in:'.implode(',', array_keys($this->signatory_pages)),
                'position' => 'sometimes|nullable|max:255',
                'division' => 'sometimes|nullable|max:255',
                'department' => 'sometimes|nullable|max:255',
                ]);
            }
            elseif($option == 'announcements')
            {
                $update_data = [
                'announcement' => $request->get('announcement'),
                'start_date' => $request->get('start_date'),
                'end_date' => $request->get('end_date'),
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
                ];

                $rules = [
                'announcement' => 'required',
                'start_date' => 'required',
                'end_date' => 'required'
                ];
            }
            elseif($option == 'trainings')
            {
                $update_data = array_merge($update_data, [
                'training_type' => $request->get('training_type')
                ]);

                $rules = array_merge($rules, [
                'training_type' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_training_types(), 'id'))
                ]);
            }
            elseif($option == 'bank_accounts')
            {
                $update_data = array_merge($update_data, [
                'bank_id' => $request->get('bank'),
                'bank_account' => $request->get('bank_account')
                ]);

                $rules = array_merge($rules, [
                'bank' => 'required|in:'.implode(',', $this->get_value_by($this->list_banks(), 'id')),
                'bank_account' => 'required|max:255',
                ]);
            }

            $this->validate_request($request->all(), $rules, $attribute_name);

            if($option == 'plantilla_items')
            {
                $data = $this->check_exist_salary_grade($request->get('salary_grade_'), $request->get('salary_step'));
                $update_data['salary_amount'] = $data->amount; 
            }
            elseif($option == 'office_suspensions')
            {
                $update_data['start_date'] = $request->get('start_date');   
                $update_data['end_date'] = $request->get('end_date');
                $hr_24 = date("H:i", strtotime($request->get('start_time')));
                $timesplit = explode(':', $hr_24);
                $update_data['start_time'] = ($timesplit[0]*60) + ($timesplit[1]);
            }
            elseif($option == 'work_schedules')
            {
                foreach($this->day as $key => $val)
                {
                    $time_in = $this->time_to_minutes($request->get($val.'_time_in'));
                    $update_data[$val.'_time_in'] = $time_in == 0 ? null : $time_in;

                    $lunch_out = $this->time_to_minutes($request->get($val.'_lunch_out'));
                    $update_data[$val.'_lunch_out'] = $lunch_out == 0 ? null : $lunch_out;

                    $lunch_in = $this->time_to_minutes($request->get($val.'_lunch_in'));
                    $update_data[$val.'_lunch_in'] = $lunch_in == 0 ? null : $lunch_in;

                    $time_out = $this->time_to_minutes($request->get($val.'_time_out'));
                    $update_data[$val.'_time_out'] = $time_out == 0 ? null : $time_out;

                    $update_data[$val.'_strict_mid'] = $request->get($val.'_strict_mid');

                    $flexi_time = $this->time_to_minutes($request->get($val.'_flexi_time'));
                    $update_data[$val.'_flexi_time'] = $flexi_time == 0 ? null : $flexi_time;

                    $update_data[$val.'_restday'] = $request->get($val.'_restday');
                }
            }
            DB::beginTransaction();

            DB::table($option)
            ->where('id', '=', $request->get('id'))
            ->update($update_data); 
            
            DB::commit();
        }
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);

            return response(['errors' => $data], 422);
        }

        return response('success', 201);
    }
    
    public function delete_record($option, $id)
    {
        try
        {   
            $this->is_filesetup_option_exist($option);

            $this->check_exist_table_record($option, $id);

            DB::beginTransaction();

            DB::table($option)
            ->where('id', '=',$id)
            ->update([
            'deleted_at' => DB::raw('now()')
            ]); 

            DB::commit();
        }
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);

            return response(['errors' => $data], 422);
        }

        return response('success', 201);
    }
}