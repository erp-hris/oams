<?php

namespace App\Http\Traits;

use DB;

trait Attendance
{
	protected function list_employees()
	{
		return DB::table('employees')
		->join('employee_informations', 'employee_informations.employee_id', '=', 'employees.id')
		->where(function ($query) {
             $query->where('employee_informations.resign_date', '=', '0000-00-00')
                   ->orwhere('employee_informations.resign_date', null);
        })
        ->select('employees.*', 'employee_informations.id as emp_info_id')
		->orderBy('last_name','asc')
		->orderBy('first_name','asc')
		->get();
	}

	
	protected function list_employees_leave($employee_id)
	{
		if ($employee_id)
		{
			return DB::table('employees_leave')
			->join('employees', 'employees.id', '=', 'employees_leave.employee_id')
			->leftjoin('leaves', 'leaves.id', '=', 'employees_leave.leave_id')
			->select('employees_leave.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'), 'leaves.name AS leave_name')
			->where('employees_leave.is_deleted', null)
			->where('employees_leave.employee_id', '=', $employee_id)
			->get();		
		}
		else
		{
			return DB::table('employees_leave')
			->join('employees', 'employees.id', '=', 'employees_leave.employee_id')
			->leftjoin('leaves', 'leaves.id', '=', 'employees_leave.leave_id')
			->select('employees_leave.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'), 'leaves.name AS leave_name')
			->where('employees_leave.is_deleted', null)
			->get();		
		}
		
	}
	protected function list_employees_forced_leave($employee_id)
	{
		if($employee_id)
		{
			return DB::table('employees_leave')
			->join('employees', 'employees.id', '=', 'employees_leave.employee_id')
			->leftjoin('leaves', 'leaves.id', '=', 'employees_leave.leave_id')
			->select('employees_leave.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'), 'leaves.name AS leave_name')
			->where('employees_leave.is_deleted', null)
			->Where('employees_leave.force_leave', '=', 1)
			->Where('employees_leave.employee_id', '=', $employee_id)
			->get();
		}	
		else
		{
			return DB::table('employees_leave')
			->join('employees', 'employees.id', '=', 'employees_leave.employee_id')
			->leftjoin('leaves', 'leaves.id', '=', 'employees_leave.leave_id')
			->select('employees_leave.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'), 'leaves.name AS leave_name')
			->where('employees_leave.is_deleted', null)
			->Where('employees_leave.force_leave', '=', 1)
			->get();
		}
	}
	

	protected function list_employees_cto($employee_id)
	{
		if($employee_id)
		{
			return DB::table('employees_cto')
			->join('employees', 'employees.id', '=', 'employees_cto.employee_id')
			->select('employees_cto.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'))
			->where('employees_cto.is_deleted', null)
			->where('employees_cto.employee_id', '=',$employee_id)
			->get();	
		}
		else
		{
			return DB::table('employees_cto')
			->join('employees', 'employees.id', '=', 'employees_cto.employee_id')
			->select('employees_cto.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'))
			->where('employees_cto.is_deleted', null)
			->get();	
		}
	}

	protected function list_employees_change_shift($employee_id)
	{

		if($employee_id)
		{
			return DB::table('employees_change_shift')
			->join('employees', 'employees.id', '=', 'employees_change_shift.employee_id')
			->leftjoin('work_schedules', 'work_schedules.id', '=', 'employees_change_shift.work_schedule_id')
			->select('employees_change_shift.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'), 'work_schedules.name AS work_schedule_name')
			->where('employees_change_shift.is_deleted', null)
			->where('employees_change_shift.employee_id', '=', $employee_id)
			->get();	
		}
		else
		{
			return DB::table('employees_change_shift')
			->join('employees', 'employees.id', '=', 'employees_change_shift.employee_id')
			->leftjoin('work_schedules', 'work_schedules.id', '=', 'employees_change_shift.work_schedule_id')
			->select('employees_change_shift.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'), 'work_schedules.name AS work_schedule_name')
			->where('employees_change_shift.is_deleted', null)
			->get();	
		}
	}

	protected function list_employees_overtime($employee_id)
	{
		if($employee_id)
		{
			return DB::table('employees_overtime')
			->join('employees', 'employees.id', '=', 'employees_overtime.employee_id')
			->select('employees_overtime.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'))
			->where('employees_overtime.is_deleted', null)
			->where('employees_overtime.employee_id', '=', $employee_id)
			->get();
		}	
		else
		{
			return DB::table('employees_overtime')
			->join('employees', 'employees.id', '=', 'employees_overtime.employee_id')
			->select('employees_overtime.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'))
			->where('employees_overtime.is_deleted', null)
			->get();
		}
	}

	protected function list_employees_office_authority($employee_id)
	{
		if($employee_id)
		{
			return DB::table('employees_absence')
			->join('employees', 'employees.id', '=', 'employees_absence.employee_id')
			->leftjoin('absences', 'absences.id', '=', 'employees_absence.absence_id')
			->select('employees_absence.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'), 'absences.name AS absence_name')
			->where('employees_absence.is_deleted', null)
			->where('employees_absence.employee_id', '=', $employee_id)
			->get();
		}	
		else
		{
			return DB::table('employees_absence')
			->join('employees', 'employees.id', '=', 'employees_absence.employee_id')
			->leftjoin('absences', 'absences.id', '=', 'employees_absence.absence_id')
			->select('employees_absence.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'), 'absences.name AS absence_name')
			->where('employees_absence.is_deleted', null)
			->get();
		}
	}

	protected function list_employees_leave_monetization($employee_id)
	{
		if($employee_id)
		{
			return DB::table('employees_leave_monetization')
			->join('employees', 'employees.id', '=', 'employees_leave_monetization.employee_id')
			->select('employees_leave_monetization.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'))
			->where('employees_leave_monetization.is_deleted', null)
			->where('employees_leave_monetization.is_deleted', '=', $employee_id)
			->get();
		}	
		else
		{
			return DB::table('employees_leave_monetization')
			->join('employees', 'employees.id', '=', 'employees_leave_monetization.employee_id')
			->select('employees_leave_monetization.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'))
			->get();
		}
	}

	protected function list_leave_policy_group()
	{
		return DB::table('leave_policy_group')
		->where('leave_policy_group.is_deleted', null)
		->get();	
	}

	protected function list_overtime_policy_group()
	{
		return DB::table('overtime_policy_group')
		->where('overtime_policy_group.is_deleted', null)
		->get();	
	}

	protected function list_leave_policy()
	{
		return DB::table('leave_policy')
		->leftjoin('leave_policy_group', 'leave_policy_group.id', '=', 'leave_policy.leave_policy_group_id')
		->leftjoin('leaves', 'leaves.id', '=', 'leave_policy.leave_id')
		->where('leave_policy.is_deleted', null)
		->select('leave_policy.*','leave_policy_group.name AS leave_policy_group_name','leaves.name AS leave_name')
		->get();	
	}

	protected function list_overtime_policy()
	{
		return DB::table('overtime_policy')
		->where('overtime_policy.is_deleted', null)
		->get();	
	}

	protected function get_application_data($table,$id)
	{
		return DB::table($table)
		->where($table.'.id', '=', $id)
		->join('employees', 'employees.id', '=', $table.'.employee_id')
		->select($table.'.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'))
		->first();
	}

	protected function get_policy_data($table, $id)
	{
		return DB::table($table)
		->where($table.'.id', '=', $id)
		->first();	
	}

	protected function get_employee_information($id)
	{
		return DB::table('employee_informations')
		->where('employee_id', '=', $id)
		->join('employees', 'employees.id', '=', 'employee_informations.employee_id')
		->leftjoin('positions', 'positions.id', '=', 'employee_informations.position_id')
		->leftjoin('divisions', 'divisions.id', '=', 'employee_informations.division_id')
		->leftjoin('departments', 'departments.id', '=', 'employee_informations.department_id')
		->select('employee_informations.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name," ",employees.middle_name) as employee_name'), 'employee_informations.id AS empinfo_id', 'positions.name AS position_name', 'divisions.name AS division_name', 'employees.employee_number', 'departments.name AS department_name')
		->first();	
	}

	protected function get_employee_name($id)
	{
		return DB::table('employees')
		->where('id', '=', $id)
		->select(DB::raw('CONCAT(employees.last_name,", ",employees.first_name," ",employees.middle_name) as employee_name'))
		->first();
	}

	protected function get_attendance_logs($biometrics_id, $month, $year)
	{
		return DB::connection('mysql2')->table('time_entry')
		->where('biometrics_id', '=', $biometrics_id)
		->whereMonth('attendance_date', '=', $month)
		->whereYear('attendance_date', '=', $year)
		->orderBy('utc')
		->get();		
	}
	protected function get_adjusted_attendance_logs($id, $month, $year)
	{
		return DB::table('employees_attendance')
		->where('employee_id', '=', $id)
		->whereMonth('attendance_date', '=', $month)
		->whereYear('attendance_date', '=', $year)
		->orderBy('attendance_date')
		->get();		
	}

	protected function list_work_schedules()
	{
		return DB::table('work_schedules')
		->get();	
	}

	protected function get_data($table,$id)
	{
		return DB::table($table)
		->where($table.'.id', '=', $id)
		->first();
	}

	protected function get_today_minute($utc)
	{
        $baseline = strtotime(date("Y-m-d",$utc));
        return intval(($utc - $baseline) / 60);
    }

    protected function time_format($time,$format = 1)
    {
    	if ($time > 0) {
    		if ($format == 1)
    		{
    			if ($time > 0) {
		           	$hr = explode(".",$time / 60)[0];
		           	if ($hr <= 9) {
		              	$hr = "0".$hr;
		           	}
		           	$mod_min = ($time % 60);
		           	if ($mod_min <= 9) {
		              	$mod_min = "0".$mod_min;
		           	}
		           	return $hr.":".$mod_min;   
		        } else {
		        	return "00:00";
		        }
    		}
    		else
    		{
    			$military_time 	= $this->time_format($time);
		    	$time_arr 		= explode(":", $military_time);
		    	$hr 			= $time_arr[0];
		    	$minute 		= $time_arr[1];
		    	$stamp 			= "";
		    	if (intval($hr) > 11) {
		    		if ($hr == "12") {
		    			return $hr.":".$minute." PM";	
		    		} else {
		    			$hr 	= intval($hr);
		    			$new_hr = $hr - 12;
		    			if ($new_hr <= 9) $new_hr = "0".$new_hr;
		    			return $new_hr.":".$minute." PM";	
		    		}
		    	} else {
		    		return $hr.":".$minute." AM";
		    	}		
    		}
    	} else {
    		return "00:00";
    	}
    }

    protected function date_difference($date_1 , $date_2 , $differenceFormat = '%a' ) 
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);
        $interval = date_diff($datetime1, $datetime2);
        return $interval->format($differenceFormat);
    }

    protected function get_application_employees_leave($id)
    {
    	return DB::table('employees_leave')
		->leftjoin('leaves', 'leaves.id', '=', 'employees_leave.leave_id')
		->where('employee_id', '=', $id)
    	->where('status', '=', '2')
		->where('employees_leave.is_deleted', null)
		->get();
    }
    protected function get_application_employees_absence($id)
    {
    	return DB::table('employees_absence')
		->leftjoin('absences', 'absences.id', '=', 'employees_absence.absence_id')
		->where('employee_id', '=', $id)
    	->where('status', '=', '2')
		->where('employees_absence.is_deleted', null)
		->get();
    }

    protected function get_application_employees_overtime($id)
    {
    	return DB::table('employees_overtime')
		->where('employee_id', '=', $id)
    	->where('status', '=', '2')
		->where('employees_overtime.is_deleted', null)
		->get();
    }

    protected function get_application_employees_cto($id)
    {
    	return DB::table('employees_cto')
		->where('employee_id', '=', $id)
    	->where('status', '=', '2')
		->where('employees_cto.is_deleted', null)
		->get();
    }

    protected function list_office_suspensions()
	{
		return DB::table('office_suspensions')
		->where('office_suspensions.deleted_at', null)
		->get();
	}

	protected function check_log($employee_id, $attendance_date, $entry_kind)
	{
		return DB::table('employees_attendance')
		->where('employee_id', '=', $employee_id)
		->where('attendance_date', '=', $attendance_date)
		->where('entry_kind', '=', $entry_kind)
		->first();
	}

	protected function get_equivalent_in_minutes($value)
	{
		return DB::table('working_minutes_conversion')
		->where('minutes', '=', $value)
		->first();
	}

	protected function get_equivalent($value)
	{
		if ($value != "" || $value != 0) {
           	if ($value <= 60) {
              	$EQDay = $this->get_equivalent_in_minutes($value);
              	return $EQDay->equivalent; 
           	} else {
              	$initial = 0;
              	$whole = explode(".",$value / 60)[0];
              	$rem = $value % 60;
              	for ($i=1; $i <= $whole; $i++) { 
                 	$EQDay = $this->get_equivalent_in_minutes(60);
                 	$initial = $initial + $EQDay->equivalent;
              	}
              	$EQDay = $this->get_equivalent_in_minutes($rem);
              	$total = $EQDay->equivalent + $initial;
              	return number_format($total,3);
           	}
        } else {
           	return "0";
        }
	}

	protected function count_application($table)
	{
		return DB::table($table)->where($table.'.status', '=', '1')->count();	
	}

	protected function list_credit_balance($id)
	{
		return DB::table('employees_credit_balance')
		->where('employee_id', '=', $id)
		->get();	
	}

	protected function get_employee_leave($id)
	{
		return DB::table('employees_leave')
		->where('employees_leave.id', '=', $id)
		->join('employees', 'employees.id', '=', 'employees_leave.employee_id')
		->join('employee_informations', 'employee_informations.employee_id', '=', 'employees_leave.employee_id')
		->leftjoin('positions', 'positions.id', '=', 'employee_informations.position_id')
		->leftjoin('divisions', 'divisions.id', '=', 'employee_informations.division_id')
		->leftjoin('departments', 'departments.id', '=', 'employee_informations.department_id')
		->leftjoin('leaves', 'leaves.id', '=', 'employees_leave.leave_id')
		->select()
		->select('employees_leave.*', 'leaves.name AS leave_name', 'positions.name AS position_name', 'divisions.name AS division_name', 'employees.employee_number', 'departments.name AS department_name', 'employees.last_name', 'employees.first_name', 'employees.middle_name')
		->first();
	}
}