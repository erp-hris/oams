<?php

namespace App\Http\Traits;

use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

trait Page
{
	protected $module;

    protected $success = 'success';

    protected $javascript_void = 'javascript:void(0);';

    protected $open_tag_tools = '<div class="tools"><button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button><div role="menu" class="dropdown-menu" x-placement="bottom-start">';

    protected $close_tag_tools = '</div></div>';

	protected function validate_request($requests, $rules, $attributes = null, $IsString = 'no')
    {
        $validator = Validator::make($requests, $rules);
        if($attributes) $validator->setAttributeNames($attributes);

        if($IsString == 'yes')
        {
            if($validator->fails()) throw new Exception(implode(" ",$validator->messages()->all()));
        }
        else
        {
            if($validator->fails()) throw new Exception(json_encode($validator->errors()));
        }

        return true;
    }

    protected function get_value_by($objects, $field, $return_array = array())
    {
    	foreach($objects as $object): $return_array[] = $object->$field; endforeach;

    	return $return_array;
    }

    protected function time_to_minutes($time)
    {
        $to_24hrs = date("H:i", strtotime($time));
        $split_time = explode(':', $to_24hrs);
        
        return ($split_time[0]*60) + ($split_time[1]);
    }

    protected function check_format_ids($idno, $length, $seperate = null, $str_pad = null, $str_to = null, $delimiter = '-')
    {
        $idno = str_replace($delimiter, '', $idno);
        
        if(strlen($idno) !== $length && $str_pad == null)
        {
            $idno = '';
        }
        else
        {
            if($str_pad) $idno = str_pad($idno, $str_pad, "0", $str_to);
            if($seperate) $idno = implode($delimiter, str_split($idno, $seperate));
        }

        return $idno;
    }

    protected function UserAuthReturnID($id)
    {
        if(Auth::user()->isUser())
        {
            return Auth::user()->employee_id;
        }
        else
        {
            return $id;
        } 
    }
}