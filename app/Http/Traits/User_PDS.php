<?php

namespace App\Http\Traits;

use DB;

trait User_PDS
{
	protected function get_employee($id)
	{
		return DB::table('employees')
		->where('id', '=', $id)
		->first();
	}

	protected function list_employee_children($id)
	{
		return DB::table('employee_children')
		->where('employee_id', '=', $id)
		->get();
	}

	protected function list_employee_education($id)
	{
		return DB::table('employee_education')
		->leftjoin('schools', 'schools.id', '=', 'employee_education.school_id')
		->leftjoin('courses', 'courses.id', '=', 'employee_education.course_id')
		->select('employee_education.*', 'schools.name AS school_name', 'courses.name AS course_name')
		->where('employee_id', '=', $id)
		->orderBy('education_level', 'asc')
		->get();
	}

	protected function list_employee_eligibility($id)
	{
		return DB::table('employee_eligibility')
		->leftjoin('eligibilities', 'eligibilities.id', '=', 'employee_eligibility.eligibility_id')
		->select('employee_eligibility.*', 'eligibilities.name AS eligibility_name')
		->where('employee_id', '=', $id)
		->get();
	}

	protected function list_employee_workexperience($id)
	{
		return DB::table('employee_work_experience')
		->leftjoin('positions', 'positions.id', '=', 'employee_work_experience.position_id')
		->leftjoin('appointment_status', 'appointment_status.id', '=', 'employee_work_experience.appointment_status_id')
		->select('employee_work_experience.*', 'positions.name AS position_name', 'appointment_status.name AS appointment_status_name')
		->where('employee_id', '=', $id)
		->get();
	}

	protected function list_employee_voluntary_work($id)
	{
		return DB::table('employee_voluntary_work')
		->leftjoin('organizations', 'organizations.id', '=', 'employee_voluntary_work.organization_id')
		->select('employee_voluntary_work.*', 'organizations.name AS organization_name')
		->where('employee_id', '=', $id)
		->get();
	}

	protected function list_employee_training($id)
	{
		return DB::table('employee_training')
		->leftjoin('trainings', 'trainings.id', '=', 'employee_training.training_id')
		->select('employee_training.*', 'trainings.name AS training_name')
		->where('employee_id', '=', $id)
		->get();
	}

	protected function list_employee_other_info($id)
	{
		return DB::table('employee_other_info')
		->where('employee_id', '=', $id)
		->get();
	}

	protected function list_employee_reference($id)
	{
		return DB::table('employee_reference')
		->where('employee_id', '=', $id)
		->get();
	}

	

	protected function count_employee_by($id)
	{
		return DB::table('employees')->where('employees.id', '=', $id)->count();
	}
}