<?php

namespace App\Http\Traits;

use Carbon\Carbon;
use DB;
use Exception;

trait Dashboard
{
	protected function get_time($employee_id,$entry_kind,$date)
	{
		return DB::table('time_entry')
		->where('employee_id', '=', $employee_id)
		->where('attendance_date', '=', $date)
		->where('entry_kind', '=', $entry_kind)
		->where('time_entry.deleted_at', null)
		->first();	
	}

	protected function check_employee_number($employee_number)
	{
		return DB::table('employees')
		->where('employees.employee_number', '=', $employee_number)
		->where('employees.deleted_at', null)
		->count();			
	}

	protected function get_data_by($employee_number)
	{
		return DB::table('employees')
		->where('employees.employee_number', '=', $employee_number)
		->where('employees.deleted_at', null)
		->first();			
	}

	protected function check_employee($employee_id)
	{
		return DB::table('employees')
		->where('employees.id', '=', $employee_id)
		->count();			
	}

	protected function check_user($employee_id)
	{
		return DB::table('users')
		->where('users.employee_id', '=', $employee_id)
		->count();			
	}
	

	protected function list_departments()
	{
		return DB::table('departments')
		->where('departments.deleted_at', null)
		->get();		
	}

	protected function list_sectors()
	{
		return DB::table('sectors')
		->where('sectors.deleted_at', null)
		->get();		
	}

	protected function list_divisions()
	{
		return DB::table('divisions')
		->where('divisions.deleted_at', null)
		->get();		
	}

	protected function time_format($time,$format = 1)
    {
    	if ($time > 0) {
    		if ($format == 1)
    		{
    			if ($time > 0) {
		           	$hr = explode(".",$time / 60)[0];
		           	if ($hr <= 9) {
		              	$hr = "0".$hr;
		           	}
		           	$mod_min = ($time % 60);
		           	if ($mod_min <= 9) {
		              	$mod_min = "0".$mod_min;
		           	}
		           	return $hr.":".$mod_min;   
		        } else {
		        	return "00:00";
		        }
    		}
    		else
    		{
    			$military_time 	= $this->time_format($time);
		    	$time_arr 		= explode(":", $military_time);
		    	$hr 			= $time_arr[0];
		    	$minute 		= $time_arr[1];
		    	$stamp 			= "";
		    	if (intval($hr) > 11) {
		    		if ($hr == "12") {
		    			return $hr.":".$minute." PM";	
		    		} else {
		    			$hr 	= intval($hr);
		    			$new_hr = $hr - 12;
		    			if ($new_hr <= 9) $new_hr = "0".$new_hr;
		    			return $new_hr.":".$minute." PM";	
		    		}
		    	} else {
		    		return $hr.":".$minute." AM";
		    	}		
    		}
    	} else {
    		return "00:00";
    	}
    }

    protected function get_attendance($id, $month, $year)
	{
		return DB::table('time_entry')
		->where('employee_id', '=', $id)
		->whereMonth('attendance_date', '=', $month)
		->whereYear('attendance_date', '=', $year)
		->orderBy('attendance_date')
		->get();		
	}

	protected function get_employee_data($employee_id)
	{
		return DB::table('employees')
		->where('id', '=', $employee_id)
		->first();
	}

	protected function get_employee_list($department_id = null)
	{
		if($department_id)
		{
			return DB::table('employees')
			->join('departments', 'departments.id', '=', 'employees.department_id')
			->where('employees.department_id', '=', $department_id)
			->select('employees.*', 'departments.name as department_name')
			->orderBy('last_name','asc')
			->get();	
		}
		else
		{
			return DB::table('employees')
			->leftjoin('departments', 'departments.id', '=', 'employees.department_id')
			->select('employees.*', 'departments.name as department_name')
			->orderBy('last_name','asc')
			->get();		
		}
		
	}

	protected function check_logs($employee_id, $entry_kind, $attendance_date)
	{
		return DB::table('time_entry')
		->where('employee_id', '=', $employee_id)
		->where('attendance_date', '=', $attendance_date)
		->where('entry_kind', '=', $entry_kind)
		->count();
	}

	protected function check_time_in($employee_id, $attendance_date)
	{
		return DB::table('time_entry')
		->where('employee_id', '=', $employee_id)
		->where('attendance_date', '=', $attendance_date)
		->where('entry_kind', '=', '1')
		->count();
	}

	protected function list_users()
	{
		return DB::table('users')
		->where('users.level', '!=', '1')
		->get();
	}

	protected function check_user_account($id)
	{
		return DB::table('users')
		->where('users.id', '=', $id)
		->count();
	}

	protected function get_user_data($id)
	{
		return DB::table('users')
		->where('users.id', '=', $id)
		->first();
	}

	protected function get_attendance_logs($date_from, $date_to)
	{
		return DB::table('time_entry')
		->join('employees', 'employees.id', '=', 'time_entry.employee_id')
		->whereBetween('attendance_date', [$date_from, $date_to])
		->select('time_entry.*', 'employees.employee_number')
		->get();
		
	}
}