<div class="row">
        <div class="col-lg-12">
            <div class="card card-border-color card-border-color-primary card-table">
                <div class="card-header card-header-divider">
                    <div class="row">
                        <div class="col-lg-6">
                            <b>Importation of Attendance Logs</b>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <label>Attendance Date From</label>
                            <input type="date" class="form-control form-control-xs" name="date_from" id="date_from">
                        </div>
                        <div class="col-lg-3">
                            <label>Attendance Date To</label>
                            <input type="date" class="form-control form-control-xs" name="date_to" id="date_to">
                        </div>
                        <div class="col-lg-2 text-center">
                            <label>&nbsp;</label>
                            <br>
                            <button type="button" class="btn btn-space btn-success" id="create_file">Create File</button>
                        </div>
                        <div class="col-lg-2 text-center">
                            <label>&nbsp;</label>
                            <br>
                            <a href="#" class="btn btn-space btn-primary" id="download_file" download>Download File</a>
                        </div>
                    </div>
                    
                </div>
                <div class="card-footer">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>