	
		@if(isset($menu_name))
			@php 
				$appendClassli = array();
				if($selected_module == $menu_name) $appendClassli[] = "active";

				if(isset($is_parent)) 
				{
					$appendClassli[] = "parent";
					if($selected_module == $menu_name) $appendClassli[] = "open";
				}
				$appendClassli = implode(' ', $appendClassli);
			@endphp

			<li class="{{ $appendClassli !== null ? $appendClassli : '' }}">
              	<a href="{{ isset($url) ? url($url) : '' }}"><i class="{{ isset($icon) ? $icon : '' }}"> </i><span>{{ __('page.'.$menu_name) }}</span></a>
              	@if(isset($sub_menu))
              	<ul class="sub-menu">
              		@foreach($sub_menu as $key => $val)
              			@if(isset($val['is_parent']))
              				<li class="parent">
              					<a href="#"><span>{{ __('page.'.$val['2nd_menu_name']) }}</span></a>
              					<ul class="sub-menu">
              						@foreach($val['2nd_sub_menu'] as $key1 => $val1)
              							@if(isset($val1['is_parent']))
              								<li class="parent">
				              					<a href="#"><span>{{ __('page.'.$val1['3rd_menu_name']) }}</span></a>
				              					<ul class="sub-menu">
				              						@foreach($val1['3rd_sub_menu'] as $key2 => $val2)
				              							<li @if(isset($option)) {{ $option == $val2['menu'] ? 'class=active' : '' }} @endif><a href="{{ url($val2['url']) }}">{{ __('page.'.$val2['menu']) }}</a></li>
				              						@endforeach
				              					</ul>
				              				</li>
              							@else
              								<li @if(isset($option)) {{ $option == $val1['menu'] ? 'class=active' : '' }} @endif><a href="{{ url($val1['url']) }}">{{ __('page.'.$val1['menu']) }}</a></li>
              							@endif
              						@endforeach
              					</ul>
              				</li>
              			@else
              				<li @if(isset($option)) {{ $option == $sub_menu[$key]['menu'] ? 'class=active' : '' }} @endif><a class="2nd_sub" href="{{ url($sub_menu[$key]['url']) }}">{{ __('page.'.$sub_menu[$key]['menu']) }}</a></li>
              			@endif
              		@endforeach
	            </ul>
              	@endif
        	</li>
		@endif