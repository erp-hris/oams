@extends('layouts.master-guest')

@section('css')
    <style type="text/css">
        .event-background{
            background: url("img/clock.jpg");
            background-position: center;
            background-size: cover;
            height: 100%;
            width: 100%;
        }
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: gray;
            color: white;
            text-align: center;
            margin: 0;
            font-size: 12pt;
        }
        .title {
            color: black; 
            font-size: 20pt;
            font-weight: 600;
            margin-top: 5%;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row title">
            <div class="col-12 text-center">
                <span>Time And Attendance Online</span>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <div class="splash-container">
                    <div class="card card-border-color" style="box-shadow: 7px 7px 7px gray;">
                        <div class="card-header">
                            <span class="splash-description">Please enter your user information.</span>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                    <input id="username" type="text" placeholder="Username" autocomplete="off" class="form-control" name="username" value="{{ old('username') }}" required autofocus>
                                    @if($errors->has('username'))
                                        <ul class="parsley-errors-list filled" id="parsley-id-5">
                                            <li class="parsley-required">{{ $errors->first('username') }}</li>
                                        </ul>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input id="password" type="password" placeholder="Password" class="form-control" name="password" required>
                                    @if($errors->has('password'))
                                        <ul class="parsley-errors-list filled" id="parsley-id-5">
                                            <li class="parsley-required">{{ $errors->first('password') }}</li>
                                        </ul>
                                    @endif
                                </div>

                                <div class="form-group login-submit">
                                    <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">Sign me in</button>
                                </div>
                            </form>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
    </div>
    <div class="row footer">
        <div class="col-lg-12 text-center">
            &copy; Copyright 2020<br>
        </div>
    </div>
@endsection