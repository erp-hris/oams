@extends('layouts.master-auth')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/select2/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}"/>
@endsection

@section('content')
    <form
        role="form"
        method="POST"
        action="{{ url('upload/attendance') }}" 
        id="add_form">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <div class="widget mb-1" style="border: 2px solid #004080; box-shadow: 7px 7px 7px gray;">
                    <div class="row">
                        <div class="col-lg-6" style="color: #404040; font-size: 15pt;">
                            <b>Employee's Login</b>        
                        </div>
                        <div class="col-lg-6 text-right">
                            
                        </div>
                    </div>
                    <div class="row margin-top">
                        <div class="col-lg-3 col-4">
                            <select class="select2 select2-xs"
                                    id="month"
                                    name="month">
                                    <option value="0">MONTH</option>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                            </select>
                        </div>
                        <div class="col-lg-3 col-4">
                            <select class="select2 select2-xs criteria"
                                    id="year"
                                    name="year">
                                    <option value="0">YEAR</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                    <option value="2024">2024</option>
                            </select>
                        </div>
                    </div>
                    <div class="row margin-top">
                        <div class="col-lg-3 col-12">
                            <button type="button" class="btn btn-success" onclick="viewDTR({{$employee_id}})">
                                <span class="icon icon-left mdi mdi-print"></span>&nbsp;
                                Print DTR
                            </button>
                        </div>
                    </div>
                </div>
                <div class="widget widget-tile" style="border: 2px solid #004080; box-shadow: 7px 7px 7px gray;">

                    <div class="data-info">
                        <div class="row">
                            <div class="col-lg-12 text-left" style="color: #404040; font-size: 15pt;">
                                <b>
                                    [ {{$data->employee_number}} ] {{ $data->last_name }}, {{ $data->first_name }} {{ $data->middle_name ? $data->middle_name[0] : '' }}.
                                    
                                </b>
                            </div>
                        </div>
                        <hr>
                        <div class="row text-center">
                            <div class="col-lg-12">
                                <span id="TimeDate" style="color: #404040; font-size: 15pt;"></span>
                            </div>
                        </div>
                        <br>
                        <div class="row text-center">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-4">
                                @if($time_in)
                                    <button class="btn btn-space form-control" type="button" id="" disabled>Log In Time {{ $time_in }}</button>
                                @else
                                    <button class="btn btn-space btn-primary form-control" type="button" id="time_in" onclick="update_time(1)">TIME IN</button>
                                @endif
                                
                            </div>
                            <div class="col-lg-2">
                                
                            </div>
                            <div class="col-lg-4">
                                @if($time_in)
                                    @if($time_out)
                                        <button class="btn btn-space form-control" type="button" id="time_out" disabled>Log Out Time {{ $time_out }}</button>
                                    @else
                                        <button class="btn btn-space btn-danger form-control" type="button" id="time_out" onclick="update_time(4)">TIME OUT</button>
                                    @endif
                                @else
                                    <button class="btn btn-space form-control" type="button" id="time_out" disabled>TIME OUT</button>
                                @endif
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    
    
@endsection

@section('scripts')
    <script src="{{ asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/js/app-dashboard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.sparkline/jquery.sparkline.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        Swal = Swal.mixin({
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-secondary',
            buttonsStyling: false
        });
        $(document).ready(function(){
            App.dashboard();
            App.formElements();

            timedate();
            var today   = new Date();
            var hours   = today.getHours();
            var minutes = today.getMinutes();
            var total_minutes = parseFloat(hours) * 60;
            total_minutes += parseFloat(minutes);
            
             if(total_minutes <= 360)
            {
                $("#time_in").prop('disabled',true);
                $("#time_in").html('<span style="color:red;font-weight:600;">NOT AVAILABLE</span>');
            }
            else
            {
                if(total_minutes >= 550)
                {
                    $("#time_in").prop('disabled',true);
                    $("#time_in").html('<span style="color:red;font-weight:600;">NOT AVAILABLE</span>');
                }
            }
            @if(!$time_out)
            if(total_minutes >= 1260)
            {
                $("#time_out").prop('disabled',true);
                $("#time_out").html('<span style="color:red;font-weight:600;">NOT AVAILABLE</span>');
            }
            @endif
            $("#year").val("{{ date('Y',time()) }}").trigger('change');
            $("#month").val("{{ date('m',time()) }}").trigger('change');

            setTimeout(function(){ document.getElementById('logout-form').submit(); }, 180000);

        });
        function viewDTR(id)
        {
            var year = $("#year").val();
            var month = $("#month").val();

            var href = window.location + '/attendance/' + id + '/' + month + '/' + year + '/preview-dtr';
            window.open(href, '_blank');
        }
        function timedate() {
            const date = new Date();

            const formattedDate = date.toLocaleString("en-GB", {
              month: "short",
              day: "numeric",
              year: "numeric",
              hour: "numeric",
              minute: "2-digit",
              second: "2-digit"
            });

            var today = new Date();
            var daylist = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            var n = today.toLocaleString();
            var x = daylist[today.getDay()] + " " + formattedDate;
            $("#TimeDate").html(x);
            var t = setTimeout(timedate, 1000);




        }
        function update_time(entry_kind) {
            var today   = new Date();
            var hours   = today.getHours();
            var minutes = today.getMinutes();
            var add_class = '';
            if(entry_kind == 1)
            {
                title = 'Signing In';
                question = 'Do you want to sign in?';
                alert = 'Sign In Successfully';
            }
            else
            {
                title = 'Signing Out';
                question = 'Do you want to sign out?';
                alert = 'Sign Out Successfully';
            }
            var frm             = document.querySelector('#add_form');
            Swal.fire({
                title: title,
                text: question,
                confirmButtonText: 'Yes',
                confirmButtonClass: 'btn btn-primary',
                closeButtonClass: 'btn btn-secondary',
                cancelButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    axios.post(frm.action, {
                        'entry_kind': entry_kind,
                        'hours': hours,
                        'minutes': minutes,
                    })
                    .then((response) => {
                        console.log(response);
                        if(response.data == 'success')
                        {
                            alert = alert;
                            add_class = 'content-actions-center colored-header colored-header-primary';
                        }
                        else if (response.data == 'time_in_required')
                        {
                            alert = 'Time In Is Required.';
                            add_class = 'content-actions-center colored-header colored-header-danger';
                        }
                        else
                        {
                            alert = 'Record already exist.';
                            add_class = 'content-actions-center colored-header colored-header-danger';
                        }
                        var timerInterval = 0;
                        Swal.fire({
                            title: alert,
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: add_class,
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                    window.location.href="{{ url('/home') }}";
                                }
                        });
                        
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        }
    </script>
@endsection