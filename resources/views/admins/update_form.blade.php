
<div class="row">
    <div class="col-lg-12">
        <div class="form-group row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label class="control-label">Name:</label>
                <input type="text" class="form-control form-control-xs" name="name" id="name">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label class="control-label">Username:</label>
                <input type="text" class="form-control form-control-xs" name="username" id="username">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label class="control-label">Sector:</label>
                <select class="select2 select2-xs" name="sector_id" id="sector_id">
                    <option value="0">Select Sector</option>
                    @foreach ($sectors as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label class="control-label">Department:</label>
                <select class="select2 select2-xs" name="department_id" id="department_id">
                    <option value="0">Select Department</option>
                    @foreach ($departments as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label class="control-label">Division:</label>
                <select class="select2 select2-xs" name="division_id" id="division_id">
                    <option value="0">Select Division</option>
                    @foreach ($divisions as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>

