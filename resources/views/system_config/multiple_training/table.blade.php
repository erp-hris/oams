                
            <table id="{{ $default_table_id }}" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                <thead>
                    <tr class="text-center">
                        <th style="width: 10%;">{{ __('page.action') }}</th>
                        <th>Employee Name</th>
                        <th>Training Type</th>
                        <th>{{ __('page.start_date') }}</th>
                        <th>{{ __('page.end_date') }}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>