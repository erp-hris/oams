        
        <div class="row">
            <div class="col-lg-5">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row" id="frm_input_training_name">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">Training Name
                                </label>
                                <select class="select2 select2-xs"
                                        id="training_name"
                                        name="training_name">
                                        <option value="">{{ __('page.please_select') }}</option>
                                        @foreach($trainings as $key => $val) <option value="{{ $val->id }}">{{ $val->name }}</option> @endforeach
                                </select>
                            <div id="error-training_name"></div>
                            @if(Auth::user()->isAdmin())
                                <div class="text-right">Not Listed? <a href="{{ url('file_setup/trainings/create') }}" target="_blank">Add Training</a></div>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row" id="frm_input_training_type">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">Type of Learning and Development
                                </label>
                                @if(Auth::user()->isAdmin())
                                    <div class="pull-right"><a href="javascript:void(0);" onclick="load_training();"><span class="mdi mdi-refresh"></span>&nbsp;Populate Type</a></div>
                                @endif
                                <select class="select2 select2-xs"
                                        id="training_type"
                                        name="training_type">
                                        <option value="">{{ __('page.please_select') }}</option>
                                        @foreach($training_types as $key => $val) <option value="{{ $val->id }}">{{ $val->name }}</option> @endforeach
                                </select>
                            <div id="error-training_type"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">Start Date</label>
                                <input class="form-control form-control-xs" type="date" id="start_date" name="start_date">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">End Date</label>
                                <input class="form-control form-control-xs" type="date" id="end_date" name="end_date">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">Number of Hours</label>
                                <input class="form-control form-control-xs" type="number" id="no_of_hours" name="no_of_hours">
                            </div>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">Conducted / Sponsored By</label>
                                <input class="form-control form-control-xs" type="text" id="sponsor" name="sponsor">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="card card-border-color card-border-color-primary" style="height: 100%;">
                    <div class="card-header card-header-divider">
                        Employee List
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <select id="employee-list" multiple="multiple" name="employees[]"></select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @section('additional-scripts')
            <script type="text/javascript">
                $("#save_btn").click(function() {

                    var frm, text, title, success;

                    @if(isset($item_id))
                        frm = document.querySelector('#edit_form');
                        title = "{{ __('page.edit_'.$option) }}";
                        text = "{{ __('page.update_this') }}";
                        success = "{{ __('page.updated_successfully', ['attribute' => trans('page.'.$option)]) }}";
                    @else
                        frm = document.querySelector('#add_form');
                        title = "{{ __('page.add_'.$option) }}";
                        text = "{{ __('page.add_this') }}";
                        success = "{{ __('page.added_successfully', ['attribute' => trans('page.'.$option)]) }}";
                    @endif


                    Swal.fire({
                        title: title    ,
                        text: text,
                        confirmButtonClass: 'btn btn-primary',
                        showCloseButton: true,
                        showCancelButton: true,
                        customClass: 'colored-header colored-header-primary'
                    }).then((result) => {
                        if(result.value){
                            var formData = new FormData();

                            @foreach($default_inputs as $key => $val)
                                formData.append("{{ $val['input_name'] }}", $("#{{ $val['input_name'] }}").val());
                            @endforeach


                            axios.post(frm.action, formData)
                            .then((response) => {
                                Swal.fire({
                                    text: success,
                                    type: 'success',
                                    customClass: 'content-text-center',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then((value) => {
                                    @if(isset($item_id))
                                        window.location.href="{{ $cancel_url }}";
                                    @else
                                        Swal.fire({
                                        title: "{{ __('page.add_'.$option) }}",
                                        text: "Do you want to add another {{ trans('page.'.$option) }}?",
                                        confirmButtonClass: 'btn btn-primary',
                                        showCloseButton: true,
                                        showCancelButton: true,
                                        customClass: 'colored-header colored-header-primary'
                                        }).then((result) => {
                                            if (result.value) {
                                                @foreach($default_inputs as $key => $val) 
                                                    $('#{{ $val['input_name'] }}').val('').trigger('change') 
                                                @endforeach

                                                clearErrors()
                                            }else {
                                                window.location.href="{{ $cancel_url }}";
                                            }
                                        })
                                    @endif
                                });
                            })
                            .catch((error) => {
                                if(typeof(error.response.data.errors) == 'string') {
                                    Swal.fire({
                                    text: error.response.data.errors,
                                    type: 'warning',
                                    customClass: 'content-text-center',
                                    showConfirmButton: false
                                    });

                                    return false;
                                }

                                Swal.fire({
                                    text: "{{ __('page.check_inputs') }}",
                                    type: 'warning',
                                    customClass: 'content-text-center',
                                    showConfirmButton: false,
                                    timer: 1500
                                });

                                const errors = error.response.data.errors
                                const firstItem = Object.keys(errors)[0]
                                const firstItemDOM = document.getElementById(firstItem)
                                const firstErrorMessage = errors[firstItem][0]
                                firstItemDOM.scrollIntoView()
                                
                                clearErrors()

                                firstItemDOM.insertAdjacentHTML('afterend', `<div class="text-danger">${firstErrorMessage}</div>`)
                                firstItemDOM.classList.add('border', 'border-danger')
                            });
                           
                            function clearErrors() {
                                const errorMessages = document.querySelectorAll('.text-danger')
                                errorMessages.forEach((element) => element.textContent = '')

                                const formControls = document.querySelectorAll('.form-control')
                                formControls.forEach((element) => element.classList.remove('border', 'border-danger'))

                            }
                        }
                    })
                });
            </script>
        @endsection