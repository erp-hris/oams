                
            <table id="{{ $default_table_id }}" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                <thead>
                    <tr class="text-center">
                        <th style="width: 10%;">{{ __('page.action') }}</th>
                        <th>{{ __('page.quotation') }}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>