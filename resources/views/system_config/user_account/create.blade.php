@extends('layouts.master-auth')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/select2/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endsection

@section('content')
    @include('others.main_content', [
    'option' => $option, 
    'title' => 'add_'.$option, 
    'has_icon' => 'mdi mdi-plus-circle',
    'has_file' => $file,
    'has_footer' => 'yes', 
    'isSave' => 'yes', 
    'cancel_url' => $cancel_url,
    'has_frm' => 'yes',
    'frm_method' => 'POST',
    'frm_action' => $frm_action,
    'frm_id' => 'add_form'
    ])
</div>
@endsection

@section('scripts')
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

   	@yield('additional-scripts')

   	<script type="text/javascript">
        $(document).ready(function(){
            App.init();
            App.formElements();
      	});
    </script>
@endsection