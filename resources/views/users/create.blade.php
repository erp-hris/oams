@extends('layouts.master-auth')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/select2/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('alertifyjs/css/alertify.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}">
    
    <link rel="stylesheet" href="{{ asset('beagle-assets/lib/sweetalert2/sweetalert2.min.css') }}" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/material-design-icons/css/material-design-iconic-font.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/multiselect/css/multi-select.css') }}"/>
    <link rel="stylesheet" href="{{ asset('beagle-assets/css/app.css') }}" type="text/css"/>
@endsection

@section('content')

    @include('others.main_content', [
    'option' => $option, 
    'title' => $title, 
    'has_icon' => $icon,
    'has_file' => $file,
    'has_footer' => 'yes', 
    'isSave' => 'yes', 
    'has_frm' => 'yes',
    'cancel_url' => $cancel_url,
    'frm_method' => 'POST',
    'frm_action' => $action_url,
    'frm_id' => 'add_form'
    ])
@endsection

@section('scripts')
    <script src="{{ asset('/axios/axios.min.js') }}"></script>
    <script src="{{ asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/lib/fuelux/js/wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/js/app-form-wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/lib/sweetalert2/sweetalert2.min.js') }}" type="text/javascript"></script>

    @yield('additional-scripts')
    <script src="{{ asset('beagle-assets/lib/multiselect/js/jquery.multi-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/lib/quicksearch/jquery.quicksearch.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/js/app-form-multiselect.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        Swal = Swal.mixin({
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-secondary',
            buttonsStyling: false
        });
        $(document).ready(function() {
            App.init();
            App.formElements();
            App.wizard();
            

            
            $("#save_btn").click(function () {
                var frm = document.querySelector('#add_form');
                Swal.fire({
                    title: "Adding new employee",
                    text: "Do you want to add this record?",
                    confirmButtonText: 'Proceed',
                    confirmButtonClass: 'btn btn-primary',
                    closeButtonClass: 'btn btn-secondary',
                    cancelButtonClass: 'btn btn-secondary',
                    showCloseButton: true,
                    showCancelButton: true,
                    customClass: 'colored-header colored-header-primary'
                }).then((isSave) => {
                    if (isSave) {
                        axios.post(frm.action, {
                            employee_number : $("#employee_number").val(),
                            last_name : $("#last_name").val(),
                            first_name : $("#first_name").val(),
                            middle_name : $("#middle_name").val(),
                            department_id : $("#department_id").val(),
                            sector_id : $("#sector_id").val(),
                            division_id : $("#division_id").val(),
                        })
                        .then((response) => {
                            console.log(response);
                            var timerInterval = 0;
                            Swal.fire({
                                title: "The Employee has been added successfully!",
                                html: 'I will close in <strong></strong> seconds.',
                                timer: 1000,
                                customClass: 'content-actions-center',
                                buttonsStyling: true,
                                onOpen: function() {
                                    swal.showLoading();
                                    timerInterval = setInterval(function () {
                                        swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                    }, 100);
                                },
                                onClose: function() {
                                    clearInterval(timerInterval);
                                }
                            }).then(function (result) {
                                if ( result.dismiss === swal.DismissReason.timer ) {
                                    window.location.href="{{ url('user') }}";
                                }
                            });
                        })
                        .catch((error) => {
                            if(typeof(error.response.data.errors) == 'string') {
                                Swal.fire({
                                text: error.response.data.errors,
                                type: 'warning',
                                customClass: 'content-text-center',
                                showConfirmButton: false
                                });

                                return false;
                            }
                        });
                    }
                });
                return false;
            });
        });
    </script>
@endsection