<nav class="navbar navbar-expand fixed-top be-top-header event-background">
    <div class="container-fluid" style="background-color: #666666;">
        <div class="be-navbar-header ">
            <a href="" >
                <img src="{{ asset('beagle-assets/img/logo.png') }}" alt="Logo" height="65px" width="100%">
            </a>
        </div>
        <div class="page-title"><span>Time And Attendance Online</span></div>
        <div class="be-right-navbar">
            <ul class="nav navbar-nav float-right be-user-nav">
                <li class="nav-item dropdown">
                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><img src="{{ URL::asset('beagle-assets/img/avatar.png') }}" alt="Avatar"><span class="user-name">{{ Auth::user()->name }}</span></a>
                    <div role="menu" class="dropdown-menu">
                        <div class="user-info">
                            <div class="user-name">
                                @if(Auth::user()->isAdmin())
                                    {{ Auth::user()->name }}
                                @elseif(Auth::user()->isUser())
                                    @php    
                                        $user = Auth::user()->Employee()->first();
                                        if($user)
                                        {
                                            $name = implode(' ', [$user->last_name,  $user->first_name]);    
                                        }
                                        else
                                        {
                                            $name = '';
                                        }
                                        
                                    @endphp

                                    {{ $name }}
                                @endif
                            </div>
                            <div class="user-position online">Available</div>
                        </div>
                        
                        <a href="{{ route('logout') }}"  class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <span class="icon mdi mdi-power"></span>&nbsp;{{ __('page.logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
